'use strict';

const mailserver = require('./../controllers/mail-server');

module.exports = function (app, express) {

  let api = express.Router();

  api.post('/send', function (req, res) {
    mailserver.post(req, res);
  });

  return api;

};
