'use strict';

var request  = require('request');


module.exports = function (app, express) {

  let api = express.Router();

  api.get('/year', function (req, res) {

    let url = 'https://www.carqueryapi.com/api/0.3/?cmd=getYears';

    request(url, function (error, response, body) {
      if(error) {
      } else {

        let response = JSON.parse(body),
          years = [];
        for(let i = response['Years'].min_year; i <= response['Years'].max_year; i++) {
          years.push({ year : i.toString() });
        }

        res.send(years);

      }
    });

  });

  api.get('/brands', function (req, res) {
    let url = 'https://www.carqueryapi.com/api/0.3/?cmd=getMakes&year=' + req.body.year;

    request(url, function (error, response, body) {
      if(error) {
      } else {
        let response = JSON.parse(body),
          brand = [];
        response = response['Makes'];

        for(let i = 0; i < response.length; i++) {
          brand.push(
            { id : response[i].make_id,
              display: response[i].make_display,
              country: response[i].make_country,
            });
        }

        res.send(brand);

      }
    });

  });

  api.get('/models', function (req, res) {
    let url = 'https://www.carqueryapi.com/api/0.3/?cmd=getModels&make=' + req.query.brand + '&year=' + req.query.year;

    request(url, function (error, response, body) {
      if(error) {
      } else {
        let response = JSON.parse(body),
          models = [];
        response = response['Models'];

        for(let i = 0; i < response.length; i++) {
          models.push(
            { id : response[i].model_make_id,
              display: response[i].model_name,
            });
        }

        res.send(models);

      }
    });

  });

  api.get('/spec', function (req,res) {
    let url = 'https://www.carqueryapi.com/api/0.3/?&cmd=getTrims&model='+ req.query.model + '&year='+ req.query.year;
    request(url, function (error, response, body) {
      if(error) {
      } else {
        let response = JSON.parse(body),
          specs = [];
        response = response['Trims'];

        for(let i = 0; i < response.length; i++) {
          specs.push(
            { id: response[i].model_id,
              brand: response[i].make_display,
              display: response[i].model_name,
              engine: response[i].model_engine_cc,
              fuel: response[i].model_engine_fuel,
              cyl: response[i].model_engine_cyl,
              horses:  response[i].model_engine_power_ps,
              weignt:  response[i].model_weight_kg,
              model:  response[i].model_body,
              trans:  response[i].model_transmission_type,
              drive:  response[i].model_drive,
            });
        }

        res.send(specs);

      }
    });
  });

  return api;

};