'use strict';

var News = require('./../controllers/news');
var Page = require('./../controllers/page');
var multer = require('multer');
var path = require('path');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'client/src/uploads/news')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
});

var upload = multer({ storage: storage});

module.exports = function (app, express) {
  let api  = express.Router();

  api.post('/page/post', upload.single('file'), function (req, res) {
    Page.post(req, res);
  });

  api.get('/page/list', function (req, res) {
    Page.list(req, res);
  });

  api.get('/page/id', function (req, res) {
    Page.getOne(req,res);
  });

  api.put('/page/page', function (req, res) {
    Page.put(req, res);
  });

  api.get('/list', function (req, res) {
    News.list(req, res);
  });

  api.get('/id', function (req, res) {
    News.getOne(req,res);
  });

  api.post('/news', upload.single('file'), function (req, res) {
    News.post(req, res);
  });

  api.post('/img', upload.single('file'), function (req, res) {
  });

  api.put('/news', upload.single('file'), function (req, res) {
    News.put(req, res);
  });

  api.delete('/news', function (req, res) {
    News.delete(req, res);
  });

  return api;
};

