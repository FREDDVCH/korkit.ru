'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var PageSchema = new Schema({
  title: { type: String, require:true },
  body: { type: Array, require:true },
  imageUrl: { type: String },
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Page', PageSchema);