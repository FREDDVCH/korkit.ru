'use strict';
var mongoose = require('mongoose'),
  News = require('./../model/news'),
  path = require('path');

exports.post = function (req, res) {
  var news = new News({
    title: req.body.title,
    body: req.body.body,
    imageUrl: '/uploads/news/' + req.file.filename
  });

  news.save({}, function (err, news) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(news);
  });
};

exports.put = function (req, res) {
  News.findOne({ _id: req.body.id }, function (error, doc) {


    if (error) {
      res.send(error);
      return;
    }

    doc.title = req.body.title;
    doc.body = req.body.body;
    doc.imageUrl = req.file ? '/uploads/news/' + req.file.filename : doc.imageUrl;

    doc.save( function (err) {
    });

    res.send({
      success: true,
      status: 'succeess',
      message: 'Успешно обновлен!'
    })

  });

};

exports.delete = function (req, res) {
  News.findOne({ _id: req.query.id })
    .remove(function (err, news) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(news);
  });
};

exports.list = function (req, res) {
  News.find({}, function (err, news) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(news);
  })
};

exports.getOne = function (req, res) {
  News.findOne({ _id: req.query.id }, function (err, news) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(news);
  })
};