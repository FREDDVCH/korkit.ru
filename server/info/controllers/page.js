'use strict';
var mongoose = require('mongoose'),
  Page = require('./../model/page'),
  path = require('path');

exports.post = function (req, res) {
  var page = new Page({
    title: req.body.title,
    body: req.body.body,
  });

  page.save({}, function (err, page) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(page);
  });
};

exports.put = function (req, res) {
  Page.findOne({ _id: req.body.id }, function (error, doc) {


    if (error) {
      res.send(error);
      return;
    }

    doc.title = req.body.title;
    doc.body = req.body.body;

    doc.save( function (err) {
    });

    res.send({
      success: true,
      status: 'succeess',
      message: 'Успешно обновлен!'
    })

  });

};

exports.delete = function (req, res) {
  Page.findOne({ _id: req.query.id })
    .remove(function (err, page) {
      if (err) {
        res.send(err);
        return;
      }
      res.send(page);
    });
};

exports.list = function (req, res) {
  Page.find({}, function (err, page) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(page);
  })
};

exports.getOne = function (req, res) {
  Page.findOne({ _id: req.query.id }, function (err, page) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(page);
  })
};