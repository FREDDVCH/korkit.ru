'use strict';

var Customer = require('../model/customer.js'),
  Order = require('../model/order'),
  User = require('../../user/models/user'),
  Price = require('../model/price'),
  https = require('https');;
  // Part = require('../../price-parts-catalog/model/price-parts'),
  // config = require('./../../../config'),
  // Analogs = require('../../price-parts-catalog/model/analogs'),
  // fs = require('fs'),
  // lread = require('readline');

https.post = require('https-post');
const sms_api_key = '808112AB-85BE-42D4-C850-68E4CFB09132';

module.exports = function (app, express) {

  let api = express.Router();

  api.get('/delivery', function (req, res) {
    Price.find({}, function (err, docs) {
      if (err) {
        res.send(err)
      }
      res.send(docs);
    });
  });

  api.put('/delivery', function (req, res) {


    Price.findOneAndUpdate({}, req.body, { upsert: true }, function (err, docs) {

      if (!err) {

        if (!docs) {
          // Create it
          docs = new Price(req.body);
        }

        docs.save(function (err) {
          if (!err) {
            res.send(docs);
          } else {
            throw err;
          }

        });

      }
    });

  });


  api.get('/price', function (req, res) {
    Price.find({}, function (err, docs) {
      if (err) {
        res.send(err)
      }
      res.send(docs);
    });
  });

  api.put('/price', function (req, res) {


    Price.findOneAndUpdate({}, req.body, { upsert: true }, function (err, docs) {

      if (!err) {

        if (!docs) {
          // Create it
          docs = new Price(req.body);
        }

        docs.save(function (err) {
          if (!err) {
            res.send(docs);
          } else {
            throw err;
          }

        });

      }
    });

  });

  api.put('/order', function (req, res) {

    let orderId = req.body._id;

    let data = {
      "items": req.body.items,
      "status": req.body.status,
      "orderLenght" : req.body.orderLenght,
      "cost": req.body.cost
    };

    Order.update({_id: orderId}, data, function (err, order) {
      if (err) {
        res.send({message: 'Ошибка изменения записи..'});
        return;
      }
      res.json({
        success:true,
        message: "Успешное изменение заказа покупателя",
      })
    });

    function getIds(array) {
      let ids = [];
      array.forEach(function (item) {
        ids.push(item._id);
      });
      return ids;
    }

  });

  api.post('/order', function (req,res) {

        let order = new Order();

        if (req.body.guest) {
          order.name = req.body.name;
          order.guestInfo = {
            tel: req.body.tel,
            phone: req.body.phone,
            email: req.body.email
          };
        } else {
          order.user = req.body.user;
        }

        order.defaultStorage = req.body.defaultStorage;
        order.orderNumber = req.body.number;
        order.items = req.body.items;
        order.orderLenght = req.body.orderLenght;
        // suppliersItem = req.body.supl ? req.body.supl  = null;
        order.cost = req.body.cost;


    order.save(function (err, order) {
      if (err) {
        res.send(err);
        return;
      }
      if (req.body.guest) {
        let url = `https://sms.ru/sms/send?api_id=${sms_api_key}&to=${'7' + req.body.phone.replace(/^8/,'')}&msg=Vash+zakaz+nomer+${order.orderNumber}+na+summu:+${order.cost}+rub&json=1`;
        https.post(url, function(res,body){ });
      }

      if (!req.body.guest) {
      let orderId = order._id;
          User
            .findOne( { _id: req.body.user }, function (err, customer) {
              if (err) {
                res.send( { message: 'Проблема с поиском покупателя...' } )
              } else {

                let data = customer;
                data.orders.push(orderId);

                let url = `https://sms.ru/sms/send?api_id=${sms_api_key}&to=${'7' + data.phone.replace(/^8/,'')}&msg=Vash+zakaz+nomer+${order.orderNumber}+na+summu:+${order.cost}+rub&json=1`;
                https.post(url, function(res,body){ });

                User.update( { _id: customer._id }, data, function (err, customer) {
                  if (err){
                    res.send(err);
                    return;
                  }
                  res.json({
                    success:true,
                    message: "Успешное добавление заказа покупателю",
                  })
                })
              }
            });
        }
    })

  });

  api.post('/add-customer', function (req,res) {

    let customer = new Customer({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      address: req.body.adress,
      email: req.body.email,
      phone: req.body.phone,
      percent: req.body.percent,
      orders: []
    });

    customer.save(function (err) {
      if (err){
        res.send(err);
        return;
      }
      res.json({
        success:true,
        message: "Успешное добавление покупателя на сайт",
      })
    })

  });

  api.put('/customer', function (req,res) {
    User.findOneAndUpdate({ _id: req.body._id }, req.body, function (err, user) {
      if (err) {
        res.send(err);
        return;
      }
      res.send(user);
    })
  });

  api.get('/customer-stat', function (req,res) {

    let pageSize = parseInt(req.query.pageSize),
      page = parseInt(req.query.page),
      nextPage = parseInt(req.query.page) + 1,
      prevPage = parseInt(req.query.page) === 0 ? parseInt(req.query.page) : parseInt(req.query.page) - 1,
      storage = req.query.storage,
      query = {};

      if (storage !== 'all') {
        query = { defaultStorage: storage };
      }

    Customer
      .count(query ,function (err, docs) {
        if(err){
          res.send({message: err});
          return;
        }
        res.send(
          {
            count: docs,
            pageIs: page,
            pageSizeIs: pageSize,
            prev: prevPage,
            next: nextPage
          }
        );
      });

  });

  api.get('/order-list', function (req,res) {

    let pageSize = parseInt(req.query.pageSize),
        page = parseInt(req.query.page),
        q = req.query.q,
        storage = req.query.storage,
        query = q !== undefined ? { user : q } : {};

        if (storage !== 'admin') {
          query.defaultStorage = storage;
        }


    Order
      .find(query, function (err, list) {

        if(err) {
          res.send({message: "Список не найден" });
          return;
        }

        res.json(list);

      })
      .limit(pageSize)
      .skip(pageSize*page)
      .populate('user')
      // .populate('items')
      .exec();

  });

  api.get('/order', function (req,res) {

    let id = req.query.id;

    // const aggregatorOpts = [{
    //   $lookup: {
    //     from: 'servicerequests',
    //     localField: 'services.servicerequest_id',
    //     foreignField: '_id',
    //     as: 'workplan'
    //   }
    // },{
    //   $unwind: "$items"
    // },
    //   {
    //     $group: {
    //       _id: "$items._id",
    //       count: { $sum: 1 }
    //     }
    //   }
    // ];

    // Order
    //   .aggregate([{
    //       $unwind: "$items"
    //     },
    //     { "$group": { "_id": id, count: { $sum: 1 }  } }
    //   ]).
    //   then(function (res) {
    //   });
        // [
        //   { "$match": { "to": user } },
        //   { "$sort": { "date": 1 } },
        //   { "$group": {
        //       "_id": "from",
        //       "to": { "$first": "$to" },
        //       "message": { "$first": "$message" },
        //       "date": { "$first": "$date" },
        //       "origId": { "$first": "$_id" }
        //     }},
        //   { "$lookup": {
        //       "from": "users",
        //       "localField": "from",
        //       "foreignField": "_id",
        //       "as": "from"
        //     }},
        //   { "$lookup": {
        //       "from": "users",
        //       "localField": "to",
        //       "foreignField": "_id",
        //       "as": "to"
        //     }},
        //   { "$unwind": { "path" : "$from" } },
        //   { "$unwind": { "path" : "$to" } }
        // ]
        // function(err,results) {
        //   if (err) throw err;
        //   return results;
        // }

    Order
      .find( { _id : id })
        .populate('user')
        // .populate('items')
        .exec(function (err, item) {

          if (err) {
            res.send({message: 'Ошибка обработки запроса...'});
          }
          res.json(item);

        })

  });

  api.get('/order-stat', function (req,res) {

    let pageSize = parseInt(req.query.pageSize),
      page = parseInt(req.query.page),
      nextPage = parseInt(req.query.page) + 1,
      prevPage = parseInt(req.query.page) === 0 ? parseInt(req.query.page) : parseInt(req.query.page) - 1,
      query = {},
      storage = req.query.storage;

      if (storage !== 'admin') {
        query.defaultStorage = storage;
      }


    Order
      .find(query, function (err, stat) {

        if (err) {
          res.send({message: "Список не найден"});
          return;
        }
        let done = 0,
          archive = 0,
          pending = 0,
          sum = 0,
          orderStatuse = {};

        stat.forEach((item)=>{
          if(item.status === 'done')
            done++;
          if(item.status === 'pending')
            pending++;
          if(item.status === 'archive')
            archive++;

          sum++;
        });

        orderStatuse.done = done;
        orderStatuse.pending = pending;
        orderStatuse.archive = archive;
        orderStatuse.sum = sum;

        res.send(
          {
            count: stat,
            pageIs: page,
            pageSizeIs: pageSize,
            prev: prevPage,
            next: nextPage,
            orderStatuses: orderStatuse
          }
        );

      })

  });

  api.get('/customer-list', function (req,res) {

    let pageSize = parseInt(req.query.pageSize),
        page = parseInt(req.query.page),
        storage = req.query.storage,
        query = {};
        if (storage !== 'admin') {
          query = { defaultStorage: storage };
        }

    User
      .find(query)
      .limit(pageSize)
      .skip(pageSize*page)
      .populate('orders')
      .exec(function (err, list) {
        if(err) {
          res.send({message: "Список не найден" });
          return;
        }
        res.json(list);
      });

  });

  api.get('/customer' , function (req,res) {
    User
      .find({ _id: req.query.id })
      .populate('orders')
      .exec(function (err,items) {
        if (err){
          res.send({message: 'Что то пошло не так...'})
        }
        res.json(items);
      })
  });

  return api;

};
