'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var PriceSchema = new Schema({
  berg: { type: Number, trim: true },
  careta: { type: Number, trim: true },
  stuz: { type: Number, trim: true  },
  bergDeliveryTime: { type: Number, trim: true, default: 4, require: true  },
  caretaDeliveryTime: { type: Number, trim: true, default: 0, require: true  },
  stuzDeliveryTime: { type: Number, trim: true, default: 0, require: true   },
  sale: { type: Number, trim: true, default: 1  }
});

module.exports = mongoose.model('Price', PriceSchema);