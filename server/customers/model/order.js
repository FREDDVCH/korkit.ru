'use strict';

var mongoose = require('mongoose'),
    Parts = require('../../price-parts-catalog/model/price-parts'),
    User = require('./../../user/models/user'),
    Schema = mongoose.Schema;

var OrderSchema = new Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    name: {type: String},
    orderNumber: { type: String, require:true },
    status: {type: String, default: 'pending' },
    // items: [{type: mongoose.Schema.Types.ObjectId, ref: 'Parts'}],
    items: {type: Array, default: []},
    created: { type: Date, default: Date.now },
    suppliersItem: {type: Array, default: []},
    orderLenght: {type: Number, default: 0},
    defaultStorage: { type: String },
    cost: { type: Number },
    guestInfo: { type: Object }
});

module.exports = mongoose.model('Order', OrderSchema);