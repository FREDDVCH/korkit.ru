'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Order = require('../model/order');

var CustomerSchema = new Schema({
    firstName: { type: String, require: true, trim: true },
    lastName: { type: String, require: true, trim: true },
    address: { type: String },
    email: { type: String, require:true, trim: true},
    phone: { type: String,require:true, trim:true },
    orders: [ {type: mongoose.Schema.Types.ObjectId, ref: 'Order'} ],
    garage: { type: Array, default:[] }
});

module.exports = mongoose.model('Customer', CustomerSchema);