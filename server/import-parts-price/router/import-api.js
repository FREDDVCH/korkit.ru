'use strict';

var Parts = require('../../price-parts-catalog/model/price-parts'),
    Storage = require('../../price-parts-catalog/model/storage'),
    Analogs = require('../../price-parts-catalog/model/analogs'),
    fs = require('fs'),
    es = require('event-stream'),
    lread = require('readline');


module.exports = function (app, express) {

    let api = express.Router();

    // api.post('/import-to-site',function(req,res) {
    //
    //     var lineNum = 0,
    //         exportArray,
    //         status = 200,
    //         message = {};
    //
    //     let lr = lread.createInterface({
    //         input: fs.createReadStream('price/short-price.csv')
    //     });
    //
    //     lr.on('line', function (line) {
    //
    //         lineNum++;
    //         let str = line;
    //         let arr = line.split('&');
    //         var analogsArr = arr[2].split(';');
    //
    //         let strPrice = arr[5],
    //             repl = strPrice.replace(/\s/g,""),
    //             iInt = parseInt(repl);
    //
    //         let obj = {
    //             id: arr[0],
    //             name: arr[1],
    //             analogs: arr[2],
    //             brand: arr[3],
    //             counts: arr[4],
    //             price: iInt,
    //             storage: arr[6]
    //         };
    //
    //         var storage = new Storage({
    //             name: obj.storage,
    //             count: obj.counts,
    //             price: obj.price
    //         });
    //
    //         if (analogsArr && analogsArr !== undefined) {
    //             let analogArray = [];
    //
    //             analogsArr.forEach(function (vin) {
    //                 let analog = new Analogs({
    //                     vin: vin,
    //                 });
    //                 analogArray.push(analog)
    //             });
    //
    //             obj.analogs = analogArray;
    //         }
    //
    //         console.log(obj.price);
    //
    //         var part = new Parts({
    //             name: obj.name,
    //             price: obj.price,
    //             vin: obj.id,
    //             analogs: obj.analogs,
    //             stocks: [storage],
    //             brand: obj.brand
    //         });
    //
    //         // part.markModified(stocks);
    //
    //         console.log(part);
    //
    //         part.save(function (err) {
    //             if (err) {
    //                 status = 400;
    //                 message = {
    //                     success: false,
    //                     message: 'Ошибка, Импорт приостановлен!'
    //                 };
    //                 res.send(err);
    //                 return;
    //             } else {
    //                 console.log(message);
    //                 status = 200;
    //                 message = {
    //                     success: true,
    //                     message: 'Успешное добавление запчасти на сайт!'
    //                 };
    //             }
    //         });
    //
    //     });
    //
    //     lr.on('close',function () {
    //         console.log('end!');
    //         res.status(status).json(message);
    //     });
    // });

    return api;

};
