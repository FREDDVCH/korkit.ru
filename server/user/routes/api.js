var User = require('../models/user'),
  Ticked = require('../models/ticked'),
  config = require('./../../../config'),
  secretKey = config.secretKey,
  jsonwebtoken = require('jsonwebtoken'),
  userCode = Math.ceil((Math.random()*(9999 - 1000) + 1000)),
  timer,
  request = require('request'),
  https = require('https');

  https.post = require('https-post');

const accountSid = 'AC5241d689a1361979bc1e379edb9011be';
const authToken = '9e167aa0b1ed4396262e93dd2f7a86e9';
const client = require('twilio')(accountSid, authToken);
const sms_api_key = '808112AB-85BE-42D4-C850-68E4CFB09132';
var user;

function createToken(user) {
  var token = jsonwebtoken.sign({
    _id: user._id,
    firstName: user.firstName,
    lastName: user.lastName,
    username: user.username,
    defaultStorage: user.defaultStorage,
    phone:user.phone,
    email:user.email
  }, secretKey, {
    expiresIn : 3600*60*24
  });

  return token;
}

module.exports = function (app, express) {

  let api  = express.Router();

  api.get('/recovery', function (req,res) {
    User.find({ username: req.query.num }, function (err, user) {
      if (user.length === 0) {
        res.send({
          message: 'Такого пользователя не существует',
          status: 'warning'
        });
        return;
      }
      if (user.length > 0) {
        let msg = 'Pass+' + user[0].ps;
        let url = `https://sms.ru/sms/send?api_id=${sms_api_key}&to=${'7' + user[0]['phone'].replace(/^8/,'')}&msg=${msg}&json=1`;
        https.post(url, function(res,body){ });
        res.send({
          message: 'СМС с паролем отправлен на ваш телефон',
          status: 'success'
        })
      }
    });
  });


  api.post('/verify', function (req, res) {

    clearInterval(timer);
    userCode = Math.ceil((Math.random()*(9999 - 1000) + 1000));

    var timer = setInterval(function () {
      userCode = Math.ceil((Math.random()*(9999 - 1000) + 1000));
    }, 120000);

    if (req.body.name && req.body.password) {
      user = new User({
        firstName: req.body.name,
        lastName: '',
        username: req.body.username,
        password: req.body.password,
        phone:req.body.tel,
        email:req.body.email,
        ps: req.body.password,
        defaultStorage: req.body.defaultStorage
      });
    }

    let msg = 'Code+' + userCode;
    let url = '';
    if (req.body.name && req.body.password) {
      url = `https://sms.ru/sms/send?api_id=${sms_api_key}&to=${'7' + user.phone.replace(/^8/, '')}&msg=${msg}&json=1`;
    }
    if (req.body.number) {
      url = `https://sms.ru/sms/send?api_id=${sms_api_key}&to=${'7' + req.body.number.replace(/^8/,'')}&msg=${msg}&json=1`;
    }


    https.post(url, function(res,body){ });

    // request.post({
    //   url: url,
    //   headers: {
    //
    //   }
    // }, function (error, response, body) {
    //
    //
    //
    // })

    // client.messages.create(
    //   {
    //     body: 'Добро пожаловать на Korkit.ru. Номер подтверждения: ' + userCode,
    //     to: '+7' + req.body.tel.replace(/^8/,''),
    //     from: '+15744061327'
    //   },
    //   (err, message) => {
    //
    //     if (!err) {
    //
    //
    //       res.json({
    //         status: 'success',
    //         message: 'Вам отправлен код в СМС'
    //       });
    //       return;
    //     }
    //     res.json({
    //       success: false,
    //       status: 'error',
    //       message: 'Проблемы с сервером отправки СМС',
    //       body: err
    //     })
    //   }
    // );

    //
    // res.json({
    //   status: 'success',
    //   message: 'Вам отправлен код в СМС'
    // })

  });

  api.post('/next', function (req,res) {

    if (parseInt(req.body.code, 10) === parseInt(userCode, 10)) {
      clearInterval(timer);
      if (user) {
        let token = createToken(user);
        user.save(function (err) {
          if(err) {
            res.json({status: 'error', success:false, message: 'Что то пошло не так! Попробуйте позднее', msg: err});
            return;
          }
          res.json({
            success:true,
            message: "Вы успешно зарегистрировались на сайте",
            token: token
          });
        });
      } else {
        res.json({
          success:true,
          message: "Вы успешно зарегистрировались на сайте"});
      }
    } else {
      clearInterval(timer);
      res.json({status: 'error', success:false, message: 'Вы ввели не правильный код доступа к сайту, попробуйте еще раз'});
    }
  });

  api.post('/signup', function (req,res) {

    let user = new User({
      firstName: req.body.name,
      lastName: '',
      username: req.body.username,
      password: req.body.password,
      phone:req.body.tel,
      email:req.body.email
    });

    let token = createToken(user);

    user.save(function (err) {
      if(err){
        res.send(err);
        return;
      }
      res.json({
        success:true,
        message: "Вы успешно зарегистрировались на сайте",
        token: token
      });
    });
  });

  api.get('/users', function (req,res) {
    User.find({}, function (err, users) {
      if(err){
        res.send(err);
        return;
      }
      res.json(users);
    });
  });

  api.post('/login', function (req,res) {
    User.findOne({
      username: req.body.username,
    }).select('name username password').exec(function (err, user) {
      if (err) throw err;
      if(!user){
        res.send({message: "Такой пользователь не существует"});
      } else if(user){
        var validPassword = user.comparePassword(req.body.password);
        if (!validPassword){
          res.send({message: "Пароль или логин не верный!"});
        } else {
          var token = createToken(user);
          res.json({
            success:true,
            message: "Вы успешно вошли в систему",
            token: token
          });
        }
      }
    });
  });
  // message system

  api.put('/ticked', function (req,res) {

    Ticked.updateMany(
      {
        orderId: req.body.orderId
      },
      {
        $set: {
          status: req.body.status
        }
      }
    ).exec(function (err, docs) {
      if (err) {
        res.send(err);
        return;
      }
      res.send(docs);
    });
  });
  api.post('/ticked', function (req,res) {
    let ticked = new Ticked({
      name: req.body.name,
      body: req.body.body,
      clientId: req.body.clientId,
      orderId: req.body.orderId,
      isAnswer: req.body.isAnswer ? req.body.isAnswer : false,
    });

    ticked.save(function (err, ticked) {
      if (err) {
        res.send(err);
        return;
      }
      res.send(ticked);
    })

  });

  api.get('/tickeds', function (req,res) {

    var storage = req.query.storage,
      query = {};

    if (storage !== 'admin') {
      query.defaultStorage = storage;
    }
    Ticked.aggregate([
      { "$match": query },
      { "$sort": { date: 1 } },
      { "$lookup": {
          from: "orders",
          localField: "orderId",
          foreignField: "_id",
          as: "orderDocs"
        }
      },
      { "$lookup": {
          from: "users",
          localField: "clientId",
          foreignField: "_id",
          as: "user"
        }
      },
      { "$group" :
          { _id: {
              order: "$orderId"
            },
            client: {$first: "$clientId"},
            isAnswer: {$first: "$isAnswer"},
            status: {$first: "$status"},
            orderDocs: {$first:"$orderDocs"},
            user: {$first: "$user"},
            firstDate: { $first: "$date"},
            count: { $sum: 1 } } }

    ]).exec(function (err,group) {
      if (err) {
        return;
      }
      var stat = {
        closed: 0,
        open: 0,
        all: 0
      };

      group.forEach(function (item) {
        if(item.status === false){
          stat.closed++;
        }
        if(item.status === true) {
          stat.open++;
        }
        stat.all++;
      });

      group.push(stat);
      res.send(group);

    })
  });

  api.get('/ticked', function (req,res) {
    Ticked.find({ _id: req.query._id }, function (err, ticked) {
      if (err) {
        res.send(err);
      }
      res.send(ticked);
    })
  });

  api.get('/ticked-client', function (req,res) {
    Ticked.findOne({ clientId: req.query.clientId })
      .populate('clientId')
      .populate('orderId')
      .exec(function (err, ticked) {
        if (err) {
          res.send(err);
        }
        res.send(ticked);
      })
  });

  api.get('/ticked-order', function (req,res) {
    Ticked.find({ orderId: req.query.orderId })
      .populate('clientId')
      .populate('orderId')
      .exec(function (err, ticked) {
        if (err) {
          res.send(err);
        }
        res.send(ticked);
      })
  });

  api.put('/user', function (req,res) {
    User.findOneAndUpdate({ _id: req.body._id }, req.body, function (err, user) {
      if (err) {
        res.send(err);
        return;
      }
      res.send(user);
    })
  });

  //middleware-Проверка токена доступа

  api.use(function (req,res,next) {
    let token = req.token || req.param('token') || req.headers['x-access-token'];
    //Проверка есть токени или нет
    if(token){
      jsonwebtoken.verify(token,secretKey, function (err, decoded) {
        if(err){
          //
          res.status(403).send({success: false, message:"Ошибка аутентификации"})
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      res.status(403).send({success: false, message:"Нет токена для доступа"})
    }
  });

  // Доступ к ...

  api.get('/me',function (req,res) {
    res.json(req.decoded);
  });

  api.put('/me', function (req,res) {
    User.findOneAndUpdate({ _id: req.body._id }, req.body, function (err, user) {
      res.send(user);
      // if (!user){
      //
      //   res.send(err);
      //   return;
      // } else {
      //   user = req.body;
      //   user.save(function (err) {
      //       if(err){
      //
      //       } else {
      //
      //       }
      //   })
      // }
    })
  });

  api.get('/user',function (req,res) {


    User.find({ _id: req.query.id }, function (err, user) {
      if(err){
        res.send(err);
        return;
      }
      res.send(user);
    });
  });

  return api
};