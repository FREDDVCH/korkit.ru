var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var TickedSchema = new Schema({
  status: {type: Boolean, default: true},
  name: {type: String, require: true},
  isAnswer: {type: Boolean, default: false},
  body: {type: String, require: true},
  clientId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  orderId: {type: mongoose.Schema.Types.ObjectId, ref: 'Order'},
  date: { type: Date, default: Date.now },
  defaultStorage: { type: String }
});

module.exports = mongoose.model('Ticked', TickedSchema);