var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');

var UserSchema = new Schema({
    firstName: { type: String, require: true, trim: true },
    lastName: { type: String, require: true, trim: true },
    address: { type: String },
    defaultStorage: { type: String },
    email: { type: String, require:true, trim: true},
    phone: { type: String,require:true, trim:true },
    orders: [ {type: mongoose.Schema.Types.ObjectId, ref: 'Order'} ],
    garage: { type: Array, default:[] },
    username: { type: String, required:true, index:{ unique: true } },
    percent: { type: Number, default: 5 },
    password: { type: String, required:true, select: false },
    date: { type: Date, default: Date.now },
    ps: { type: String }
});

//Хешируем пароль пользователя перед передачей в БД

UserSchema.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) return next();
    bcrypt.hash(user.password, null, null, function (err,hash) {
        if (err) return next(err);
            user.password = hash;
        next();
    })
});

UserSchema.methods.comparePassword = function (password) {
    var user = this;
    return bcrypt.compareSync(password, user.password);
};

module.exports = mongoose.model('User', UserSchema);