'use strict';

var Berg = require('./../controllers/berg'),
  Careta = require('./../controllers/carreta');

module.exports = function (app, express) {
  let api  = express.Router();

  api.get('/1/list', function (req, res) {
    Berg.getList(req, res);
  });

  api.get('/1/cross', function (req, res) {
    Berg.getCrossList(req,res);
  });

  api.get('/2/list', function (req,res) {
    Careta.getList(req,res);
  });

  return api;
};
