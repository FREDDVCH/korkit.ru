'use strict';
var request = require('request'),
  mongoose = require('mongoose'),
  Parts = mongoose.model('Parts'),
  Price = require('../../customers/model/price'),
  API_BERG = 'fed54dd35d735526a829261bcd09cbc0b48a8d6993e6f16aaca4e224b62d376b',
  reqexp = /[/=,|]/g,
  crossSet = new Set();

exports.getList = function (req, res) {
  Price.find({}, function (err, price) {
    var priceList = {
      berg: 1.50
    };
    var deliveryTime = {};

    if(!err) {
      priceList = {
        berg: '1.' + price[0]['berg'],
        sale: price[0]['sale'] + parseInt(req.query.percent)
      };
      deliveryTime = {
        bergDeliveryTime: price[0]['bergDeliveryTime'],
      };
    }

    var qs = req.query.qs,
      url = 'http://api.berg.ru/ordering/get_stock.json?items[0][resource_article]=' + qs +'&key=' + API_BERG;

    request(url, function (error, response, body) {

      var partList = JSON.parse(body);
      if (error) {

      } else if (!partList.resources[0]) {
        res.send({
          status: 'error',
          message: 'Такой позиции не обнаружено'
        });
      } else {
        var bergList = [],
          bergOenList = partList.resources;

        bergOenList.forEach(function (item) {
          if (true) {

            var offers = item.offers,
              berg;

            berg = new Parts({
              name: item.name.replace(reqexp,' '),
              brand: item.brand.name.replace(reqexp,' '),
              minAwait: +item.average_period + deliveryTime.bergDeliveryTime,
              maxAwait: +item.assured_period + deliveryTime.bergDeliveryTime,
              description: '',
              vin: item.article,
              analogs: [],
              stocks: [],
              supplier: 'Berg',
              clientView: 'BR1',
              resource_id: item.resource_id,
            });

            if (offers) {
              offers.forEach(function (item, index) {

                var minPrice;

                item.assured_period = +item.assured_period + deliveryTime.bergDeliveryTime;
                item.average_period = +item.average_period + deliveryTime.bergDeliveryTime;

                if (index === 0) {
                  minPrice = item.price;
                  berg.stocks[0] = item;
                  berg.count = +item.quantity;
                  berg.stocks[0].price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                  berg.price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                }

                if (minPrice > item.price) {
                  minPrice = item.price;
                  berg.stocks[0] = item;
                  berg.count = +item.quantity;
                  berg.stocks[0].price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                  berg.price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                }

              });
            }
            bergList.push(berg);
          }
        });
        res.send(bergList);

      }
    });
  })
};

exports.getCrossList = function (req, res) {
  Price.find({}, function (err, price) {
    var priceList = {
      berg: 1.50
    };
    var deliveryTime = {};
    if (!err) {
      priceList = {
        berg: '1.' + price[0]['berg'],
        sale: price[0]['sale'] + parseInt(req.query.percent)
      };
      deliveryTime = {
        bergDeliveryTime: price[0]['bergDeliveryTime'],
      };
    }

    var qs = req.query.qs,
      url = !req.query.brand ? 'http://api.berg.ru/ordering/get_stock.json?items[0][resource_article]=' + qs + '&analogs=1&key=' + API_BERG : 'http://api.berg.ru/ordering/get_stock.json?items[0][resource_article]=' + qs + '&items[0][brand_name]=' + req.query.brand + '&analogs=1&key=' + API_BERG;
    request(url, function (error, response, body) {

      var partList = JSON.parse(body);
      if (error) {

      } else if (partList.warnings) {
        const bergCrosslist = partList.resources.map(item => {
          if (item.brand.name !== 'РОССИЯ') {
            return {
              brand: item.brand.name,
              article: item.article
            }
          }
        });

        res.send({
          status: 'mismatch',
          body: bergCrosslist
        })

      } else if (!partList.warnings) {

        var bergList = [],
          bergItem = partList.resources;

        bergItem.forEach(function (item, index) {
          if (item.article.replace(reqexp, '') !== qs) {

            // собираем список кроссов
            // parseCross(index, {brand: item.brand.name, vin: item.article});

            var offers = item.offers,
              berg;

            berg = new Parts({
              name: item.name.replace(reqexp, ' '),
              brand: item.brand.name.replace(reqexp,' '),
              minAwait: +item.average_period + deliveryTime.bergDeliveryTime,
              maxAwait: +item.assured_period + deliveryTime.bergDeliveryTime,
              description: '',
              vin: item.article,
              analogs: [],
              stocks: [],
              supplier: 'Berg',
              clientView: 'BR1',
              resource_id: item.resource_id,
              count: item.quantity
            });

            if (offers) {
              offers.forEach(function (item, index) {

                var minPrice;

                item.assured_period = +item.assured_period + deliveryTime.bergDeliveryTime;
                item.average_period = +item.average_period + deliveryTime.bergDeliveryTime;

                if (index === 0) {
                  minPrice = item.price;
                  berg.stocks[0] = item;
                  berg.count = +item.quantity;
                  berg.stocks[0].price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                  berg.price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                }

                if (minPrice > item.price) {
                  minPrice = item.price;
                  berg.stocks[0] = item;

                  berg.stocks[0].price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                  berg.price = Math.ceil(percentage(berg.stocks[0].price * priceList.berg, 100 - priceList.sale));
                }

              });
            }
            bergList.push(berg);
          }
        });
        res.send(bergList);
      }
    });
  })
};

exports.bergCrosslist = function (req, res) {
  return [...crossSet];
};

function percentage(num, per)
{
  return (num/100)*per;
}


async function parseCross(index ,obj) {
  if (index === 1 && crossSet.length !== 0) {
    crossSet.clear();
    crossSet.add(obj);
  }
  if (index > 1) {
    crossSet.add(obj);
  }

}