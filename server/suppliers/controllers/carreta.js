'use strict';

var request = require('request'),
  mongoose = require('mongoose'),
  Parts = mongoose.model('Parts'),
  Price = require('../../customers/model/price'),
  reqexp = /[/=,|]/g,
  _KEY = 'f2fdad8f-3493-4733-a860-ce00fc9d1809'; //ключ можно поменять в личном кабинете кареты

exports.getList = function (req, res) {
  Price.find({}, function (err, price) {
    var priceList = {
      careta: 1.50
    };

    var deliveryTime = {};

    if(!err) {
      priceList = {
        careta: '1.' + price[0]['careta'],
        sale: price[0]['sale'] + parseInt(req.query.percent)
      };
      deliveryTime = {
        caretaDeliveryTime: price[0]['caretaDeliveryTime'],
      };
    }

    var qs = req.query.qs,
      url = 'http://api.carreta.ru/v1/search/?q=' + qs + '&api_key=' + _KEY;

    request(url, function (error, response, body) {

      var partList = JSON.parse(body);

      if (error) {
        res.send({
          status: 'error',
          message: 'Запрос не был обработан, совершите запрос позднее...',
          log: error
        })
      } else {

        var caretaList = [],
          collection = [],
          temp = [];

        partList['objects'].forEach(function (part, index) {
          //
          if (!part['is_cross']) {
            var careta;
            careta = new Parts({
              name: part.name.replace(reqexp, ' '),
              brand: part.maker.replace(reqexp, ' '),
              minAwait: +part.period_min + +deliveryTime.caretaDeliveryTime,
              maxAwait: +part.period_max + +deliveryTime.caretaDeliveryTime,
              description: '',
              vin: part.code,
              analogs: [],
              stocks: [{
                quantity: parseInt(part.qty) ? +part.qty : 2,
                assured_period: +part.period_min + +deliveryTime.caretaDeliveryTime,
                average_period: +part.period_max + +deliveryTime.caretaDeliveryTime,
                price: Math.ceil(percentage(+part.price * +priceList.careta, 100 - +priceList.sale)),
                warehouse: {
                  id: part.maker + '_' + part.source
                }
              }],
              supplier: 'Careta',
              clientView: 'CR1',
              resource_id: part.maker + '_' + part.source,
              count: parseInt(part.qty) ? +part.qty : 2,
              is_cross: part.is_cross,
              price: Math.ceil(percentage(+part.price * +priceList.careta, 100 - +priceList.sale))
            });
            collection.push(careta);
          }
        });
          temp = [...collection];

          collection.forEach(el => {
            temp.forEach((item, index) => {
              if (el.brand === item.brand && el.price < item.price) {
                temp.splice(index, 1);
                return;
              }
            });
          });

        res.send(temp);

      }
    })
  });

};

function percentage(num, per)
{
  return (num/100)*per;
}