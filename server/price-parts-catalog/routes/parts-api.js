'use strict';

var Parts = require('../model/price-parts'),
    Storage = require('../model/storage'),
    path = require('path'),
    config = require('./../../../config'),
    Analogs = require('../../price-parts-catalog/model/analogs'),
    fs = require('fs'),
    lread = require('readline'),
    berg = require('../../suppliers/controllers/berg'),
    multer = require('multer'),
    storage = multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, 'price')
      },
      filename: function (req, file, cb) {
        cb(null, file.fieldname + path.extname(file.originalname))
      }
    });

var upload = multer({ storage: storage});

module.exports = function (app, express) {

    let api = express.Router();

    api.get('/check-list', function (req, res) {
      if (JSON.parse(req.query.q).length) {
        let data = JSON.parse(req.query.q),
          arrayToReq,
          message = new Set();
        arrayToReq = data.map((item) => {
          return {
            vin: item['vin'],
            brand: item['brand']
          }
        });

        Parts.find({ $or: arrayToReq }, function (err, items) {

          if (err) {
            res.send({
              status: 'error',
              message: ['Обновление не удалось, Сервер не отвечает...'],
              log: err });
            return;
          }

          items.forEach(el => {
            data.forEach( item => {
              if (item.vin === el.vin && item.brand === el.brand) {
                if (item.price !== el.price) {
                  item.price = el.price;
                  let priceChange = 'Цена на ' + item.vin + ' изменилась!';
                  message.add(priceChange);
                }
                let storage = el.stocks.find(function (el) {
                  return el.name === item.orderStorage['name'];
                });
                if (item.orderCount > storage.count) {
                  let countChange = 'На складе' + storage.name.replace('Склад', '') + ' недостаточно позиции ' + item.vin + ', установлено максимально допустимое количество';
                  if (storage.count === 0) {
                    let countChange = 'На складе' + storage.name.replace('Склад', '') + ' отсутствует позиция ' + item.vin + ', будет удалена из вашей корзины';
                  }
                  message.add(countChange);
                  item.orderStorage = storage;
                  item.orderCount = storage.count;
                  item.stocks = el.stocks;
                }
              }
            });
          });
          let response = {
            message: [...message].length === 0 ? ['Корзина актуальна, можно перейти к заказу'] : [...message],
            status: [...message].length === 0 ? 'success' : 'warning',
            result: data};


          res.send(response);
        });
      } else {
        let response = {
          message:['Корзина актуальна, можно перейти к заказу'],
          status: 'success',
          result: []
        };
        res.send(response);
      }

    });

    api.post('/price', upload.single('price'), function (req, res) {

      var stat = fs.stat('price/price.csv');

      if (req.file) {
        res.send({
          status: 'success',
          success: true,
          message: 'Файл успешно загружен',
          fileInfo: req.file,
          fileStat: stat,
        })
      } else if (err) {
        res.send({
          status: 'error',
          success: false,
          message: 'Файл не загружен, серверу не удалось записать файл в хранилище'
        })
      }
    });

    api.get('/fresh', function (req, res) {

      fs.stat('price/price.csv', function (err, stats) {
        if(err){
          res.send({
            success: false,
            message: 'Файл прайса не обнаружен на сервере!'
          })
        } else {
          res.send({
            success: true,
            fileInfo: stats
          });
        }

      });

    });
    
    api.post('/add-part', function (req,res) {

        let part = new Parts({
            name: req.body.name,
            price: req.body.price,
            description: req.body.description,
            vin: req.body.vin,
            stocks:[stocksNsk, stocksBrn, stocksStorage],
            brand: req.body.brand,
            nsk: req.body.nsk,
            biysk: req.body.biysk,
        });

        part.markModified(stocks);

        part.save(function (err) {
            if (err) {
                res.send(err);
                return;
            }
            res.json({
                success:true,
                message: "Успешное добавление запчасти на сайт",
            })
        })

    });

    api.post('/import-to-site',function(req,res) {

        var lineNum = 0,
            exportArray,
            status = 200,
            message = {},
            errorMsg = [],
            itemFromPrice,
            fileInfo,
            path ='price/price.csv';

        fs.stat('price/price.csv', function (err, stats) {
            if(err){

            } else {
                fileInfo = stats;
            }
        });


        let lr = lread.createInterface({
            input: fs.createReadStream(path)
        });
        const getPrice = function (storageArr) {
          let price = 0;

          storageArr.forEach((item)=>{
            if(item.price > price) {
              price = item.price;
            }
          });

          return price;

        };

        const getAllStorage = function(array) {

          let storageArray = [];

          for(var i = 4; i < array.length - 1; i+=3){

            let storage = new Storage({
              name: array[i],
              count: array[i+1],
              price: array[i+2],
              basketCount: 0
            });

            storageArray.push(storage);
          }

          return storageArray;

        };

        lr.on('line', function (line) {
            try {

                itemFromPrice = line;

                lineNum++;

                let arr = line.split('&'),
                    analogsArr = arr[2].split(';'),
                    storageArr = getAllStorage(arr),
                    price =  getPrice(storageArr);

                let obj = {
                    id: arr[0].replace(/\W/, "").toUpperCase(),
                    name: arr[1],
                    analogs: arr[2],
                    brand: arr[3],
                    stocks: storageArr,
                    price: price
                };
                //
                // let part = new Parts({
                //   name: obj.name,
                //   vin: obj.id,
                //   analogs: obj.analogs,
                //   brand: obj.brand,
                //   stocks: obj.stocks,
                //   price: obj.price
                // });

                if (analogsArr && analogsArr !== undefined) {
                    let analogArray = [];

                    analogsArr.forEach(function (vin) {
                        let analog = new Analogs({
                            vin: vin.replace(/\W/, "")
                        });
                        analogArray.push(analog)
                    });

                    obj.analogs = analogArray;
                }

                let query = {
                    "vin": obj.id,
                    "brand": obj.brand,
                };

                let options = {
                    upsert: true,
                };

                let data = {
                    "name": obj.name,
                    "vin": obj.id,
                    "price": obj.price,
                    "analogs": obj.analogs,
                    "brand": obj.brand,
                    "stocks": obj.stocks
                };

                Parts.findOneAndUpdate(query, {$set: data}, options, function (err, item) {
                    if (err) {
                      return err;
                    }
                    if(item) {
                    }
                });

            } catch(e) {


                errorMsg.push({
                    line: lineNum,
                    itemError: itemFromPrice
                });
            }
        });

        lr.on('close',function () {
            //
            fileInfo.count = lineNum;
            fileInfo.errorMsg = errorMsg;
            res.status(status).json(fileInfo);
        });

    });

    api.get('/stat',function (req,res) {

      let pageSize = parseInt(req.query.count),
          page = parseInt(req.query.page),
          nextPage = parseInt(req.query.page) + 1,
          prevPage = parseInt(req.query.page) === 0 ? parseInt(req.query.page) : parseInt(req.query.page) - 1;


      let searchPattern = req.query.vin,
        reg = /\W/g,
        request = `${searchPattern.replace(reg, '(\\W|\\s|)')}`,
        query = { vin: request };
      if (request === 0) {
        query = {};
      };
        // query = {
        //   $or: [
        //     {vin: {$regex: request, $options: 'i'}},
        //     {
        //       analogs:
        //         {
        //           $elemMatch:
        //             {
        //               vin:
        //                 {$regex: request, $options: 'i'}
        //             }
        //         }
        //     }
        //   ]
        // };
        Parts
          .count(query,function (err, docs) {
            if(err){
              res.send({message: err});
              return;
            }
            res.send(
              {
                count: docs,
                pageIs: page,
                pageSizeIs: pageSize,
                prev: prevPage,
                next: nextPage
              }
              );
        });

    });

    api.get('/list-parts', function (req,res) {

      let pageSize = parseInt(req.query.count),
          page = parseInt(req.query.page),
          searchPattern = req.query.vin,
          regExp = /\W|\s|\-/g,
          request = searchPattern.toUpperCase().replace(regExp,''),
          query = { vin: request };
      if (request === 0) {
        query = {};
      }
          // query = {
          //   $or: [
          //     {
          //       vin:
          //         {$regex: request, $options: 'i'}
          //         },
          //   ]
          // };

      Parts
        .find(query, function (err, parts) {

        if (err) {
          res.send({message: "Оригинальная запчасть не найдена"});
          return;
        }
        if (parts.length === 0) {
          res.send(
            {
              status: 'error',
              message: "Оригинальная запчасть не найдена"
            });
          return;
        }
        parts.map(function (part) {
          return part.stocks.forEach(function (stock) {
            stock.price = Math.ceil(stock.price - percentage(stock.price, req.query.percent));
          })
        });
        res.send(parts);

      }).limit(pageSize)
        .skip(pageSize * page)
    });

    api.get('/get-cross', function (req,res) {

      let crossArray = berg.bergCrosslist(),
        pageSize = parseInt(req.query.count),
        page = parseInt(req.query.page),
        searchPattern = req.query.vin,
        regExp = /\W|\s|\-|\./g,
        request = searchPattern.toUpperCase().replace(regExp,''),
        orRequest = [{vin: request}];

        // crossArray.forEach( item => {
        //   if(item){
        //     let cross =  item.vin.toUpperCase().replace(regExp,'');
        //     orRequest.push({ 'vin': cross });
        //   }
        //   // request += '|' + item.vin.toUpperCase().replace(regExp,'(\\W|\\s|)');
        // });
        //
        //

      // let query = { $or: { $or : orRequest }};
      //
      let query = {
        $or: [{$or: orRequest},
          {
          analogs:
            {
              $elemMatch: {$or: orRequest}
            }
          }]
      };
      //
      Parts.find(query, function (err, parts) {

        if(err) {
          res.send({message: "Аналоги для запчасти не найден" });
          return;
        }
        let filteredParts = parts.filter((item) => item.vin !== request);

        filteredParts.map(function (parts) {
          return parts.stocks.forEach(function (stock) {
            stock.price = Math.ceil(stock.price - percentage(stock.price, req.query.percent))
          })
        });

        res.json(filteredParts);

      }).limit(pageSize);
    });

    api.get('/best-price', function (req,res) {

      let searchPattern = req.query.vin,
        regExp = /\W|\s/g,
        request = searchPattern.search(regExp) === -1 ? searchPattern : `${searchPattern.toUpperCase().replace(regExp,'(\\W|\\s|)')}`,
        query = {
          $or: [
            {vin: {$regex: request, $options: 'i'}},
            {
              analogs:
                {
                  $elemMatch:
                    {
                      vin:
                        {$regex: request, $options: 'i'}
                    }
                }
            }
          ]
        };




        Parts
          .findOne(query, function (err, cheap) {

            if(err) {
              res.send({message: "Дешевая запчасть не найдена" });
              return;
            }

            res.json(cheap);

          })
          .sort('-price')
          .exec()
    });

    return api;

};
function percentage(num, per)
{
  return (num/100)*per;
}