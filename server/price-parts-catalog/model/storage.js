'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StorageSchema = new Schema({
    name: {type: String, required:true},
    price: {type: Number},
    count: {type: Number, default:0, required:true},
    basketCount: {type: Number, default:0, required:true},
    timeArrival: {type: Number}
});

module.exports = mongoose.model('Storage',StorageSchema);