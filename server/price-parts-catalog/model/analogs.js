'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AnalogSchema = new Schema({
    vin: {type: String}
});

module.exports = mongoose.model('Analog', AnalogSchema);