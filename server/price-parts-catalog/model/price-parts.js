'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PartsSchema = new Schema({
   name: {type: String, required:true},
   description: {type: String},
   vin:{type: String, required:true, trim: true},
   analogs:{type: Array},
   brand:{type: String},
   price:{type:Number},
   stocks:{type: Array},
   supplier: {type: String},
   clientView: {type: String},
   resource_id: {type: String},
   minAwait: {type: String},
   maxAwait: {type: String},
   count: {type: Number},
   is_cross: {type: Boolean}
});

module.exports = mongoose.model('Parts',PartsSchema);
