const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
});


const config = {
    entry: './client/app.js',
    output: {
        path: path.resolve('./client/src'),
        // publicPath: '/src',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            { test: /\.html$/, use: 'raw-loader',exclude: /node_modules/ },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                }),
                // loader: ExtractTextPlugin.extract('css!sass')
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [ 'file-loader' ]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [require('@babel/plugin-proposal-object-rest-spread')]
                    }
                }

            }
        ]

    },
    // devtool: "source-map",

    plugins: [
        new HtmlWebpackPlugin({template: './client/index.html'}),
        new ExtractTextPlugin("styles.css")
    ]
};

module.exports = config;