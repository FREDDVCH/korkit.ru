'use strict';
import searchParts from './searchParts';

const SearchPart = angular.module('SearchPartService',[]);

SearchPart.factory('SearchParts', searchParts);

export default SearchPart;