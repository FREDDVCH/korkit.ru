'use strict';

const searchParts = ['$rootScope','$http','$q',function ($rootScope,$http,$q) {

    let searchService = {};
    let parts = [];
    let cross = [];
    let regExp = /\W|\s|\-/g;

    searchService.getItemByVIM = function( oen, percent = 0) {

        return $http.get('/api/catalog/list-parts',{ params:{ count: 50, vin: oen, percent: percent }})
          .then(function (response) {
            let data = {};
            parts = response.data;
            data.res = parts;
            return data;
        },function (err) {

            return {message : 'Ошибка доступа'}
        });
    };

    searchService.getCrossList = function ( oen, percent = 0 ) {

        return $http.get('/api/catalog/get-cross', { params:{ count: 50,vin: oen, percent: percent}})
          .then(function (response) {
            let data = {};
            cross = response.data;
            data.res = cross;
            return data;
          },function (err) {

          })
    };

    searchService.getCheap = function ( oen, percent = 0 ) {

        return $http.get('/api/catalog/best-price', { params: {vin: oen, percent: percent} })
          .then(function (response) {
            let data = {},
              lowerPrice,
              stocks = response.data.stocks;

            data.res = response.data;
            lowerPrice = 0;

            stocks.forEach(function (item, index) {
              if(lowerPrice <= 0){
                lowerPrice = item.price;
              }
              if(item.count > 0 && item.price !== 0) {
                lowerPrice = item.price;
                data.res.stocks = [item];

              }
            });
            return data;
          }, function (err) {

          })

    };

    searchService.getSupplier = function ( oen, percent = 0 ) {

      const list = [];

      list.push($http.get('/api/suppliers/1/list',{ params:{ qs: oen, percent: percent }}));
      list.push($http.get('/api/suppliers/2/list',{ params:{ qs: oen, percent: percent }}));

      return Promise.all(list)
        .then(function (res) {
          const array = [];
          res.forEach(function (el) {

            array.push(...el.data);
          });

          return array;
        })
        .then(function (response) {
          let data = {};
          let result = response.filter(el => {
            if (parts.length > 0) {
              return !parts.some(function (part) {
                return part.brand.toUpperCase() === el.brand.toUpperCase();
              })
            } else {
              return !0;
            }
          });

          data.res = result;
          return data;
        },function (err) {

          return {message : 'Ошибка доступа'}
        });


      // return $http.get('/api/suppliers/1/list',{ params:{ qs: oen, percent: percent }})
      //   .then(function (response) {
      //     let data = {};
      //     let result = response.data.filter(el => {
      //       if (parts.length > 0) {
      //         return !parts.some(function (part) {
      //           return part.brand.toUpperCase() === el.brand.toUpperCase();
      //         })
      //       } else {
      //        return !0;
      //       }
      //     });
      //
      //     data.res = result;
      //     return data;
      //   },function (err) {
      //
      //     return {message : 'Ошибка доступа'}
      //   });
    };

    searchService.getSupplierCross = function (oen, percent = 0, brand = null) {

      return $http.get('/api/suppliers/1/cross',{ params:{ qs: oen, brand: brand, percent: percent }})
        .then(function (response) {
          let data = {};
          data.res = response.data;



          if (!data.res['status']) {
            data.res = data.res.filter(el => {

              if (cross.length > 0) {
                return !cross.some(function (part) {

                  return (part.brand.toUpperCase() === el.brand.toUpperCase() && part.vin.toUpperCase() === el.vin.replace(regExp,'').toUpperCase());
                })
              } else {

                return !0;
              }
            });
          }

          return data;
        },function (err) {

          return {message : 'Ошибка доступа'}
        });
    };

    searchService.checkList = function (q) {
      return $http.get('/api/catalog/check-list', { params: { q: q } })
    };

    return searchService

}];

export default searchParts;