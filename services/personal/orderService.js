'use strict';

const orderService = ['$rootScope', '$http', '$q',function ($rootScope,$http,$q){

  let factory = {};

  factory.createOrder = function (order) {
    return $http.post('/api/customers/order', order);
  };

  factory.getOrder = function (id) {
    return $http.get('/api/customers/order',{ params: { id: id } })
  };

  factory.getOrders = function (q, page, pageSize) {
    return $http.get('/api/customers/order-list',{ params: { q: q,  page: page, pageSize: pageSize } })
  };

  factory.checkList = function (q) {
    return $http.get('/api/customer/check-list', { params: { q: q } })
  };

  factory.checkNumber = function (num) {
    return $http.post('/api/verify', num)
  };

  return factory;

}];

export default orderService;