'use strict';

const info = ['$http', function ($http) {

  let factory = {};

  factory.getYearList = function () {
    return $http.get('/api/carInfo/year');
  };
  factory.getBrands = function (year) {
    return $http.get('api/carInfo/brands', year);
  };
  factory.getModels = function (req) {
    return $http.get('api/carInfo/models', { params: req });
  };
  factory.getSpec = function (req) {
    return $http.get('api/carInfo/spec', { params: req });
  };
  return factory
}];

export default info