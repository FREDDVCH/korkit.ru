'use strict';

const pageService = ['$http', function ($http) {
  let factory = {};

  factory.getList = function () {
    return $http.get('/api/info/page/list')
  };

  factory.getById = function (id) {
    return $http.get('/api/info/page/id', { params: { id: id } })
  };

  return factory;
}];

export default pageService;