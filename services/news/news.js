'use strict';

const newsService = ['$http', function ($http) {
  let factory = {};

  factory.getList = function () {
    return $http.get('/api/info/list')
  };

  factory.getById = function (id) {
    return $http.get('/api/info/id', { params: { id: id } })
  };

  return factory;
}];

export default newsService;