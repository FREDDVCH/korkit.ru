'use strict';

const localStorageService = function () {

  let factory = {};

  factory.setList = function (list) {
    localStorage.setItem('basket', JSON.stringify(list));
  };

  factory.setItem = function (item) {

    var items = JSON.parse(localStorage.getItem('basket'));

    if(items === null) {
      items = [];
    }

    let unique = true;

    for(let i = 0; i < items.length; i++) {
      if(items[i].name === item.name && items[i].vin === item.vin && items[i].price === item.price && items[i].orderStorage._id === item.orderStorage._id) {
        items[i].orderCount = items[i].orderCount + item.orderCount;
        unique = false;
      }
    }

    if (unique) {
      items.push(item);
    }

    localStorage.setItem('basket', JSON.stringify(items));
  };

  factory.getItems = function () {


    let basket = JSON.parse(localStorage.getItem('basket')) === null ? [] : JSON.parse(localStorage.getItem('basket')),
      promise = new Promise(resolve => resolve(basket));

     return promise;
  };

  factory.clearLocalStorage = function () {
    localStorage.removeItem('basket');
  };

  return factory;

};

export default localStorageService;