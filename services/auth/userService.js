'use strict';

const userService = ['$http', function ($http) {


  let userFactory = {};

  userFactory.recovery = function (num) {

    return $http.get('/api/recovery', { params: { num: num } });
  };

  userFactory.create = function (UserData) {
    return $http.post('/api/verify', UserData);
  };

  userFactory.sendCode = function (data) {
    return $http.post('/api/next', data)
  };

  userFactory.all = function () {
    return $http.get('/api/users');
  };

  userFactory.me = function () {
    return $http.get('/api/me');
  };

  userFactory.getUser = function (userId) {
    return $http.get('/api/user', { params: { id: userId } });
  };

  return userFactory;

  }];

export default userService