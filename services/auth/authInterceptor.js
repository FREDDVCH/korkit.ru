'use strict'
import '../auth/authToken'

const AuthInterseptor = ['$q','$location','AuthToken' ,function ($q, $location, AuthToken) {

    return {

        request: function (config) {
            let token = AuthToken.getToken();

            if (token){

                config.headers['x-access-token'] = token;
            }

            return config;
        },

        responseError: function (response) {
            if(response.status == 403)
              $location.path('#!/auth/login');
            return $q.reject(response);
        }

    };


}];

export default AuthInterseptor