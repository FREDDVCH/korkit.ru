'use strict';

const Auth = ['$http','$q','AuthToken', function ($http, $q, AuthToken ) {

    let authFactory = {};
    let user = {};
    authFactory.login = function (username, password) {



        let obj = {
            username: username,
            password: password
        };

        let config = {};

        return $http.post('/api/login', obj)
            .then(function (res) {
                AuthToken.setToken(res.data.token);
                return res;
            })

    };

    authFactory.logout = function () {
        AuthToken.setToken();
    };

    authFactory.isLoggedIn = function () {
        if(AuthToken.getToken())
            return true;
        else
            return false;
    };

    authFactory.getUser = function () {

        if(AuthToken.getToken())
            return $http.get('/api/me');
        else
            return $q.reject({ message: 'У пользователя нет токена доступа'});
    };

    authFactory.putUser = function (usr) {
      if(AuthToken.getToken())
          return $http.put('/api/me', usr);
      else
          return $q.reject({ message: 'У пользователя нет токена доступа'});
    };

    authFactory.getMeta = function () {
      if(AuthToken.getToken())
        $http.get('/api/me')
          .then((res) => {


            return res.data;
          });
      else
        return $q.reject({ message: 'У пользователя нет токена доступа'});
    };

    return authFactory;

}];

export default Auth