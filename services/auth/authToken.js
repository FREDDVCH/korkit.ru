'use strict';

const AuthTokenService = ['$window',function ($window) {
    var authTokenFactory = {};

    authTokenFactory.getToken = function () {
        let token = $window.localStorage.getItem('token');

        if (token)
            return token;
        else
            console.log('Токен не зарегистрирован')
    };

    authTokenFactory.setToken = function (token) {
        if(token)
            $window.localStorage.setItem('token',token);
        else
            $window.localStorage.removeItem('token');
    };

    return authTokenFactory;
}];

export default AuthTokenService
