'use strict';

import AuthInterceptors from './authInterceptor'
import AuthToken from './authToken'
import AuthService from './authService'
import UserService from './userService'

const AuthServices = angular.module('AuthService',[]);

AuthServices.factory('Auth', AuthService);
AuthServices.factory('AuthToken', AuthToken);
AuthServices.factory('AuthInterceptor', AuthInterceptors);
AuthServices.factory('User', UserService);

export default AuthServices
