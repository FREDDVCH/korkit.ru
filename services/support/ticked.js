'use strict';

const supportService = ['$http', function ($http) {

  let factory = {};

  factory.getListByClientId = function (id) {
    return $http.get('/api/ticked-client', { params: { clientId: id }})
  };

  factory.getListByOrderId = function (id) {
    return $http.get('/api/ticked-order', { params: { orderId: id }})
  };

  factory.postTicked = function (body) {
    return $http.post('/api/ticked', body);
  };

  factory.sendMail = function (form) {
    return $http.post('/api/mail/send', form)

  };

  return factory;

}];

export default supportService;