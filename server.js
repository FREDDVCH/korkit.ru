var  express =require('express'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    app = express(),
    mongoose = require('mongoose'),
    config = require('./config'),
    path = require('path');

mongoose.connect(config.database, {
    useMongoClient:true
});

app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());
app.use(morgan('dev'));

// var importApi = require('./server/import-parts-price/router/import-api')(app,express);
//
// app.use('/api/import',importApi);

app.use(express.static(path.join(__dirname + '/client/src')));
app.use(express.static(path.join(__dirname + '/uploads/news')));

var infoApi = require('./server/info/routes/info')(app, express),
  partsApi = require('./server/price-parts-catalog/routes/parts-api')(app, express),
  customerApi = require('./server/customers/routes/customers-api.js')(app, express),
  carApi = require('./server/car-api/route/car-api.js')(app, express),
  suppliers = require('./server/suppliers/route/suppliers.js')(app, express),
  api = require('./server/user/routes/api.js')(app, express),
  mailApi = require('./server/mail-server/routes/mail-api.js')(app, express);

app.use('/api/info', infoApi);
app.use('/api/catalog', partsApi);
app.use('/api/carInfo', carApi);
app.use('/api/mail', mailApi);
app.use('/api/customers', customerApi);
app.use('/api/suppliers', suppliers);
app.use('/api', api);

app.get('/', function (req,res) {
    res.sendFile(__dirname + '/client/src/index.html');
});

//Dashboard page

app.use(express.static(path.join(__dirname + '/dashboard/dist')));

app.get('/dashboard', function (req,res) {
    res.sendFile(__dirname + '/dashboard/dist/index.html')
});

app.listen(config.port, function (err) {
    if (err){
        console.log();
    } else {
        console.log("port 8000 is open we are listn");
    }
});