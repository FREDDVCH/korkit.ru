'use strict';
/**
 service for manipulate parts
 */
angular.module('sbAdminApp')
    .factory('PartsService', function($http) {

        var factory = {};

        factory.update = function () {
            return $http.post('api/catalog/import-to-site');
        };

        factory.postPrice = function (csv) {
          return $http.post('api/catalog/price', csv, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
          })
        };

        factory.getData = function () {
          return $http.get('api/catalog/fresh')
        };

        factory.getParts = function (vin, pageNum, pageSize) {
              //что бы получить весь список запчастей нужно передать vin = 0
              if(vin === 0 || vin === undefined){
                vin = 0;
              }
              console.log(vin);
              let promises = [];


              promises.push($http.get('api/catalog/list-parts', { params: {vin: vin, page: pageNum, count: pageSize }}));
              promises.push($http.get('api/catalog/stat',{ params: {vin: vin, page: pageNum, count: pageSize }}));

              return Promise.all(promises);

          };

          return factory;

    });