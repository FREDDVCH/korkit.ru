'use strict';

angular.module('sbAdminApp')
  .factory('PageService', function ($http) {
    var pageFactory = {};

    pageFactory.postPage = function(body) {
      return $http.post('/api/info/page/page', body)
    };

    pageFactory.updatePage = function (body) {
      return $http.put('/api/info/page/page', body);
    };

    pageFactory.getPage = function() {
      return $http.get('/api/info/page/page')
    };

    pageFactory.getOnePage = function(id) {
      return $http.get('/api/info/page/id', { params: { id: id } })
    };

    pageFactory.getList = function (page, pageSize) {
      return $http.get('/api/info/page/list', { params: { page: page, pageSize: pageSize } })
    };

    return pageFactory;

  });