'use strict';
/**
 service for manipulate parts
 */
angular.module('sbAdminApp')
  .factory('SupportService', function($http) {

    var factory = {};

    factory.getAll = function (storage = 'admin') {
      return $http.get('/api/tickeds', { params: { storage: storage } })
    };

    factory.getById = function (id) {
      return $http.get('/api/ticked', { params: { _id: id } })
    };

    factory.getByClientId = function (clientId) {
      return $http.get('/api/ticked', { params: { clientId: clientId } })
    };

    factory.getByOrderId = function (orderId) {
      return $http.get('/api/ticked-order', { params: { orderId: orderId } })
    };

    factory.postTicked = function (body) {
      return $http.post('/api/ticked', body)
    };

    factory.changeStatusTicked = function (body) {
      return $http.put('/api/ticked', body)
    };

    return factory;

  });