//show all orders
'use strict';

angular.module('sbAdminApp')
  .directive('customer-add',function(){
    return {
      templateUrl:'scripts/directives/customers/create/customer-add.html',
      restrict: 'E',
      controller: 'customerController',
      controllerAs: 'vm'
    }
  });