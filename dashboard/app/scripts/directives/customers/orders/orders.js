//show all orders
'use strict';

angular.module('sbAdminApp')
  .directive('orders',function(){
    return {
      templateUrl:'scripts/directives/customers/orders/orders.html',
      restrict: 'E',
      controller: 'adminController',
      controllerAs: 'ac',
      replace: true
    }
  });