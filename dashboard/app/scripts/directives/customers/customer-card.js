//show all orders
'use strict';

angular.module('sbAdminApp')
  .directive('customer',function(){
    return {
      templateUrl:'scripts/directives/customers/customer-card.html',
      restrict: 'E',
      controller: 'customerController',
      controllerAs: 'vm'
    }
  });