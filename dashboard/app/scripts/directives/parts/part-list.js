'use strict';

angular.module('sbAdminApp')
  .directive('partList',function(){
    return {
      templateUrl:'scripts/directives/parts/part-list.html',
      restrict: 'E',
      controller: 'MainCtrl',
      controllerAs: 'vm',
      transclude: true,
      replace: true,
    }
  });


