'use strict';

angular.module('sbAdminApp')
  .directive('notify',function(){
    return {
      templateUrl:'scripts/directives/notify/notify.html',
      restrict: 'E',
      controller: 'MainCtrl',
      controllerAs: 'mc',
      replace: true,
    }
  });

