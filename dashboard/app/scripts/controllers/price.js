'use strict';
/**
 controller for manipulate parts
 */
angular.module('sbAdminApp')
  .controller('priceController', function($scope, $position, $stateParams, $timeout, PriceService) {

    var vm = this;
    vm.bergPercent = 'Получение данных с сервера...';
    vm.caretaPercent = 'Получение данных с сервера...';
    vm.stuzPercent = 'Получение данных с сервера...';
    vm.summarySale = 'Получение данных с сервера...';

    vm.onSubmit = function () {
      var priceList = {
        berg: vm.bergPercent,
        careta: vm.caretaPercent,
        stuz: vm.stuzPercent,

        bergDeliveryTime: parseInt(vm.bergDelivery),
        caretaDeliveryTime: parseInt(vm.caretaDelivery),
        stuzDeliveryTime: parseInt(vm.stuzDelivery),
      };

      PriceService.setNewPrice(priceList)
        .then(function (items) {
        })
        .catch(function (err) {
        });

    };

    vm.onSubmitSale = function () {
      var priceList = {
        sale: vm.summarySale,
      };
      PriceService.setNewPrice(priceList)
        .then(function (items) {
        })
        .catch(function (err) {
        });
    };

    function onInit() {

      PriceService.getPrice()
        .then(function (prices) {
          vm.bergPercent = prices.data[0].berg;
          vm.caretaPercent = prices.data[0].careta;
          vm.stuzPercent = prices.data[0].stuz;
          vm.summarySale = prices.data[0].sale;
          vm.bergDelivery = prices.data[0].bergDeliveryTime;
          vm.caretaDelivery = prices.data[0].caretaDeliveryTime;
          vm.stuzDelivery = prices.data[0].stuzDeliveryTime;
        });

    }

    onInit();

  });