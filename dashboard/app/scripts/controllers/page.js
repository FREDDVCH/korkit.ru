'use strict';

angular.module('sbAdminApp')
  .controller('pageCntr', function($scope, $state, $stateParams, PageService) {
    var vm = this,
      id = $stateParams.id;

    vm.title = '';
    vm.body = [];
    vm.file;
    vm.image;

    vm.getOne = function () {
      PageService.getOnePage(id).then(function (res) {
        console.log(res.data);
        vm.image = res.data.imageUrl;
        vm.title = res.data.title;
        vm.body = res.data.body;

        vm.imageOld = res.data.imageUrl;
        vm.titleOld = res.data.title;
        vm.bodyOld = res.data.body;
      })
    };

    vm.update = function () {
      vm.sendPost(false);
    };

    vm.getList = function (page, count) {
      PageService.getList(page, count)
        .then(function (news) {
          console.log(news);
          vm.news = news.data;
        });
    };

    vm.sendPost = function (status) {
      let body = {
        id: id,
        title: vm.title,
        body: vm.body
      };
      console.log(body);
      if (status) {
        PageService.postPage(body)
          .then(function (req) {
            $state.go('dashboard.editPage');
            console.log(req.data);
          })
          .catch(function (e) {
            console.log(e);
          });
      } else {
        PageService.updatePage(body)
          .then(function (req) {
            $state.go('dashboard.editPage');
            console.log(req.data);
          })
          .catch(function (e) {
            console.log(e);
          });
      }

    };

  })
