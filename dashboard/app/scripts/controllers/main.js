'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('MainCtrl', function($scope, $position, $stateParams, CustomerService, PartsService, SupportService) {
    var vm = this,
        id = $stateParams.id,
        storage =$stateParams.storage;

    vm.orderStatus = [
      {id: '1', name: 'В ожидании', value: 'pending' },
      {id: '2', name: 'Готов', value: 'done' },
      {id: '3', name: 'Архив', value: 'archive' }
    ];

    function onInit() {
      SupportService.getAll(storage).then(
        function (messageList) {
          console.log(messageList, 'ЬУЫЫФПУ ДШЫЕ');
          vm.messagelist = messageList.data[messageList.data.length - 1];
          console.log(vm.messagelist);
        }
      )
    };

    onInit();


    vm.getOrderStat = function (page, pageSize) {
      CustomerService.getOrders(page, pageSize, storage).then(
        function (orderList) {
          console.log('get order stat');
          vm.orderStat = orderList[1].data;
          console.log(vm.orderStat.orderStatuses);
        }
      )
    };

    vm.getOrder = function () {
      console.log('id = ' + id);
      CustomerService.getOrder(id).then(
        function (order) {
          console.log(order);
          vm.order = order.data[0];
          vm.selectedStatusOrder = {id: '1', name: 'В ожидании', value: order.data[0].status};
        }
      )
    };

    vm.sum = function () {
      let sum = {};
      sum.sum = 0;
      sum.count = 0;

      vm.order.items.forEach( item => {

        sum.sum += item.price * item.orderCount;
        sum.count += item.orderCount;
      });

      return sum;
    };

    vm.editOrder = function () {
      vm.editMode = true;
    };

    vm.cancelEdit = function () {
      vm.editMode = false;
    };

    vm.saveOrder = function () {
      vm.order.status = vm.selectedStatusOrder.value;
      vm.order.cost = vm.sum().sum;
      vm.order.orderLenght = vm.sum().count;

      CustomerService.updateOrder(vm.order).then(
        function (order) {
          console.log(order.data);
        }, function (err) {
          console.log(err);
        }
      );

      vm.editMode = false;
    };

    vm.addToList = function (part, storage) {
      part = {...part, orderStorage: {...storage}, orderCount: 1};

      let unique = true;

      for(let i = 0; i < vm.order.items.length; i++) {
        if(vm.order.items[i].vin === part.vin && vm.order.items[i].price === part.price) {
          console.log('совпадение');

          vm.order.items[i].orderCount = vm.order.items[i].orderCount + 1;

          console.log(vm.order.items[i]);
          unique = false;
        }
      }

      if (unique) {
        vm.order.items.push(part);
      }

    };

    vm.removeFromList = function (id) {
      console.log('remove id is ' +  id);
      vm.order.items.forEach(function (item, index) {
        console.log(index +' ' + item._id );
        if(item._id === id) {
          console.log('got it!');
          vm.order.items.splice(index,1);
        }
      })
    };

    vm.getParts = function (vin, page, count) {

      PartsService.getParts(vin,page,count).then(function (items) {
        console.log('Directive' + items);
        vm.parts = items[0].data;
        vm.stat = items[1].data;
      });

    };

  });
