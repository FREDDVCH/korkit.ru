'use strict';
/**
    controller for manipulate parts
 */
angular.module('sbAdminApp')
    .controller('partsController', function($scope,$position,$stateParams,PartsService) {
        var vm = this;

        $scope.searchPattern = 0;

        vm.updateDB = function () {

            PartsService.update().then(function (item) {
                var time = new Date(item.data.mtime),
                    day = time.getDate(),
                    month = time.getMonth()+1,
                    year = time.getFullYear(),
                    hour = time.getHours(),
                    min = time.getMinutes();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    if (min.length < 2) min = '0' + min;
                    if (hour.length < 2) hour = '0' + hour;

                var created = 'Дата: ' + day +'/' + month + '/' + year + 'Время: ' + hour + ':' + min;
                vm.errorMsg = item.data.errorMsg;
                vm.count = item.data.count;
                vm.refresh = created;
                vm.getPriceData();
            });
        };

        vm.getPriceData = function () {
          PartsService.getData().then(function (res) {
            if (res.data.success) {
              vm.priceData = res.data;
              return;
            }
            throw res.data.message;
          }).catch(function (err) {
            vm.priceData.message = err;
          })
        };

        vm.getParts = function (vin, page, count) {

          PartsService.getParts(vin,page,count).then(function (items) {
            vm.parts = items[0].data;
            vm.stat = items[1].data;
          });

        };

        vm.postPrice = function () {
          var fd = new FormData();
          fd.append('price',  $scope.myFile);

          PartsService.postPrice(fd)
            .then(function (res) {
              if (res.data.success) {
                vm.data = res.data;
                vm.message = res.data.message;
                vm.error = '';
                $scope.myFile = undefined;
                return;
              }
              throw res.data.message;
              })
            .catch(function (err) {
              vm.message = '';
              vm.error = err;
            })

        };

    });
