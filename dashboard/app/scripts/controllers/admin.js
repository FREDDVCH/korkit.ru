'use strict';
/**
 controller for manipulate parts
 */
angular.module('sbAdminApp')
  .controller('adminController', function($scope, $position, $stateParams, $timeout ,CustomerService) {

    var vm = this,
        params = $stateParams.id ? $stateParams.id : '' ,
        storage = $stateParams.storage;
    console.log(storage, 'Контроллер админа!');

    vm.getList = function (page, pageSize) {
      CustomerService.getCustomersList(page, pageSize, storage)
        .then( function (item) {
          console.log(item);
          vm.stat = item[1].data;
          vm.list = item[0].data;
        });
    };

    vm.getUserById = function () {
      CustomerService.getCustomerById(params)
        .then( function (user) {
          vm.name = user.data.firstName;
            $timeout(function () {
              console.log(user);
              vm.customer = user.data;
              vm.name = user.data.firstName;
              $scope.asd = user.data.length;
            },5000);
        });
    };

    vm.getOrderList = function (page, pageSize) {
      CustomerService.getOrders(page, pageSize, storage).then(function (orders) {
        console.log(orders);
          vm.orders = orders[0].data;
          vm.ordersStat = orders[1].data;
      })
    }

  });
