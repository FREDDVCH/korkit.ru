'use strict';

angular.module('sbAdminApp')
  .directive('ordersList',function(){
    return {
      templateUrl:'scripts/directives/customers/orders/orders-list/order-list.html',
      restrict: 'E',
      controller: 'adminController',
      controllerAs: 'vm'
    }
  });