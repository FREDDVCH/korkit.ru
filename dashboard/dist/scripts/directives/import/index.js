//import to DB from csv file

'use strict';

angular.module('sbAdminApp')
    .directive('import',function(){
        return {
            templateUrl:'scripts/directives/import/import-block.html',
            restrict: 'E',
            controller: 'partsController',
            controllerAs: 'vm',
            replace: true
        }
    });