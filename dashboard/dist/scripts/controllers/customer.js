'use strict';
/**
 controller for manipulate parts
 */
angular.module('sbAdminApp')
  .controller('customerController', ["$scope", "$position", "$stateParams", "$timeout", "CustomerService", function($scope, $position, $stateParams, $timeout ,CustomerService) {

    var vm = this,
      params = $stateParams.id ? $stateParams.id : '' ;

    vm.name = 'Bo';
    vm.isEdit = false;

    vm.getUserById = function () {
      CustomerService.getCustomerById(params)
        .then( function (user) {
          vm.customer = user.data[0];
          vm.percent = vm.customer.percent;
          vm.firstName = vm.customer.firstName;
          vm.lastName = vm.customer.lastName;
          vm.email = vm.customer.email;
          vm.phone = vm.customer.phone;
        });
    };

    vm.editMode = function () {
      vm.isEdit = true;
    };

    vm.cancel = function() {
      vm.isEdit = false;
    };

    vm.save = function () {
      let newPercent = vm.percent,
        user = {
          _id: vm.customer._id,
          firstName: vm.firstName,
          lastName: vm.lastName,
          email: vm.email,
          phone: vm.phone,
          percent: newPercent
        };
      CustomerService.updateUser(user)
        .then( function (res) {
          return res;
        }).then( function (res) {
        CustomerService.getCustomerById(params)
          .then( function (user) {
            vm.customer = user.data[0];
            vm.percent = vm.customer.percent;
            vm.isEdit = false;
          });
      })
    }

  }]);
