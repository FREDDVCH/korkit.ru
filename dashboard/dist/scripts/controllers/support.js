'use strict';
/**
 controller for manipulate parts
 */
angular.module('sbAdminApp')
  .controller('supportController', ["$scope", "$position", "$stateParams", "$timeout", "SupportService", function($scope, $position, $stateParams, $timeout, SupportService) {

    var vm = this;
    vm.ticked = [];
    vm.message = '';
    var storage = $stateParams.storage;
    function onInit() {
      SupportService.getAll(storage)
        .then( function (res) {
          vm.tickeds = res.data;
        });
    }

    vm.getTicked = function () {
      SupportService.getByOrderId($stateParams.id)
        .then(function (res) {
          vm.ticked = res.data;
        })
    };

    vm.onSubmit = function() {
      var message = {
        name: 'Служба поддержки korkit.ru',
        body: vm.message,
        isAnswer: true,
        orderId: vm.ticked[0].orderId._id,
        clientId: vm.ticked[0].clientId._id
      };
      SupportService.postTicked(message)
        .then(function (res) {
          vm.message = '';
          return res;
        })
        .then(function () {
          SupportService.getByOrderId($stateParams.id)
            .then(function (res) {
              vm.ticked = res.data;
            })
        })
    };

    vm.open = function () {
      SupportService.changeStatusTicked({ status: true, orderId: $stateParams.id })
        .then(function (res) {
        })
        .then(function () {
          SupportService.getByOrderId($stateParams.id)
            .then(function (res) {
              vm.ticked = res.data;
            })
          SupportService.getAll(storage)
            .then( function (res) {
              vm.tickeds = res.data;
            });
        })
    };

    vm.close = function () {
      SupportService.changeStatusTicked({ status: false, orderId: $stateParams.id })
        .then(function (res) {
        })
        .then(function () {
          SupportService.getByOrderId($stateParams.id)
            .then(function (res) {
              vm.ticked = res.data;
            })
          SupportService.getAll(storage)
            .then( function (res) {
              vm.tickeds = res.data;
            });
        })
    };

    onInit();


  }]);