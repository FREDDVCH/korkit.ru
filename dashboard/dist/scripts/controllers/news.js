'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('newsCntr', ["$scope", "$state", "$stateParams", "NewsService", function($scope, $state, $stateParams, NewsService) {
    var vm = this,
      id = $stateParams.id;

    vm.title = '';
    vm.body = [];
    vm.file;
    vm.image;

    function onInit() {
    }
    vm.getOne = function () {
      NewsService.getOneNews(id).then(function (news) {
        vm.image = news.data.imageUrl;
        vm.title = news.data.title;
        vm.body = news.data.body;

        vm.imageOld = news.data.imageUrl;
        vm.titleOld = news.data.title;
        vm.bodyOld = news.data.body;
      })
    };
    vm.update = function () {
      vm.sendPost(false);
    };
    vm.getList = function (page, count) {
      NewsService.getList(page, count)
        .then(function (news) {
          vm.news = news.data;
        });
    };

    vm.createBody = function () {
      // vm.body = vm.body.replace(/\n/g,'[nr]');
      // vm.body = vm.body.replace(/\s/g,'[sp]');
    };

    vm.delete = function () {
      NewsService.delete(id)
        .then(function (res) {
          $state.go('dashboard.news');
        }).catch(function (e) {
      });
    };

    vm.sendPost = function (status) {

      vm.createBody();

      // var req = {
      //   title: vm.title,
      //   body: [vm.body],
      //   image: $scope.myFile.name
      // };

      var fd = new FormData();
      fd.append('file',  $scope.myFile);
      fd.append('title', vm.title);
      fd.append('body', vm.body);
      // NewsService.postImg(fd).then(function (res) {
      //
      // });
      if (status) {
        NewsService.postNews(fd)
          .then(function (req) {
            $state.go('dashboard.news');
          })
          .catch(function (e) {
          });
      } else {
        fd.append('bodyOld', vm.bodyOld);
        fd.append('id', id);
        NewsService.updateNews(fd)
          .then(function (req) {
            $state.go('dashboard.news');
          })
          .catch(function (e) {
          });
      }

    };

    $scope.uploadFile = function(){
      var file = $scope.myFile;
      var uploadUrl = "/savedata";
      // fileUpload.uploadFileToUrl(file, uploadUrl);
    };

    onInit();


  }]);
  // .directive('fileModel', ['$parse', function ($parse) {
  //   return {
  //     restrict: 'A',
  //     link: function(scope, element, attrs) {
  //       var model = $parse(attrs.fileModel);
  //       var modelSetter = model.assign;
  //       element.bind('change', function(){
  //         scope.$apply(function(){
  //           modelSetter(scope, element[0].files[0]);
  //         });
  //       });
  //     }
  //   };
  // }]);