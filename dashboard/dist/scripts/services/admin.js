'use strict';

angular.module('sbAdminApp')
    .factory('CustomerService', ["$http", function ($http) {

      var factory = {};

      factory.getCustomersList = function (page, pageSize, storage = 'admin') {
        let promises = [];

        promises.push($http.get('api/customers/customer-list', { params: { page: page, pageSize: pageSize, storage: storage } }));
        promises.push($http.get('api/customers/customer-stat', { params: { page: page, pageSize: pageSize, storage: storage } }));

        return Promise.all(promises);

      };

      factory.getCustomerById = function (id) {
        return $http.get('api/customers/customer', { params: { id: id } } );
      };

      factory.addCustomer = function (customer) {
        return $http.post('api/customers/customer/:id', { params: customer})
      };

      factory.updateUser = function (body) {
        return $http.put('/api/user', body)
      };

      factory.getOrder = function (id) {
        return $http.get('api/customers/order', { params: { id: id } } );
      };

      factory.getOrders = function (page, pageSize, storage = 'admin') {

        let promises = [];

        promises.push($http.get('api/customers/order-list', { params: { page: page, pageSize: pageSize, storage: storage } }));
        promises.push($http.get('api/customers/order-stat', { params: { page: page, pageSize: pageSize, storage: storage } }));

        return Promise.all(promises);

      };

      factory.updateOrder = function (order) {
        return $http.put('api/customers/order', order );
      };

      return factory;

    }]);