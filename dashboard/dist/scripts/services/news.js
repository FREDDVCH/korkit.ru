'use strict';

angular.module('sbAdminApp')
  .factory('NewsService', ["$http", function ($http) {
    var newsFactory = {};

    newsFactory.postNews = function(body) {
      return $http.post('/api/info/news', body, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      })
    };

    newsFactory.postImg = function (img) {
      return $http.post('/api/info/img', img, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      })
    };

    newsFactory.delete = function (id) {
      return $http.delete('/api/info/news', { params: { id: id } })
    };

    newsFactory.updateNews = function (fd) {
      return $http.put('/api/info/news',fd, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      });
    };

    newsFactory.getNews = function() {
      return $http.get('/api/info/news')
    };

    newsFactory.getOneNews = function(id) {
      return $http.get('/api/info/id', { params: { id: id } })
    };

    newsFactory.getList = function (page, pageSize) {
      return $http.get('/api/info/list', { params: { page: page, pageSize: pageSize } })
    };

    return newsFactory;

  }]);