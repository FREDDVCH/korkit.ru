'use strict';
/**
 service for manipulate parts
 */
angular.module('sbAdminApp')
  .factory('PriceService', ["$http", function($http) {

    var factory = {};

    factory.getPrice = function () {
      return $http.get('/api/customers/price')
    };

    factory.setNewPrice = function (data) {
      return $http.put('/api/customers/price', data)
    };

    factory.setSale = function (data) {
      return $http.put('/api/customers/price', data);
    };

    factory.getDeliveryTime = function () {
      return $http.get('/api/customers/delivery')
    };

    factory.setDeliveryTime = function (date) {
      return $http.put('/api/customers/delivery', date)
    };

    return factory;

  }]);