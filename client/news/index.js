'use strict';

import listDirective from './newsMain.news.directive';
import newsItemDirective from './newsItem.news.directive';
import newsPage from './view/news-page.news.view.html';
import NewsController from './news.cntr';
import NewsService from './../../services/news/news';

import './scss/news.scss'

const newsModule = angular.module('NewsModule', []);

newsModule.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('news',{
        abstract:true,
        url:'/news',
        template:'<ui-view></ui-view>'
    })
    $stateProvider.state('news.list',{
        url:'/news',
        data: {
            title: 'Новости от Korkit.ru'
        },
        template:'<news-list></news-list>'
    })
    $stateProvider.state('news.item',{
        url:'/news/:id',
        data: {
            title: 'Новость от Korkit.ru'
        },
        template: newsPage,
        controller: 'newsController',
        controllerAs: 'vm'
    })
}]);

newsModule.directive('newsList', listDirective);
newsModule.directive('newsItem', newsItemDirective);
newsModule.controller('newsController', NewsController);
newsModule.service('newsService', NewsService);
export default newsModule;