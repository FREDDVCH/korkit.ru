'use strict';

const newsController = ['$scope','$rootScope','$state','$element', '$stateParams', 'newsService', function ($scope,$rootScope, $state, $element, $stateParams, newsService) {

  var vm = this;
  console.log('news cntrl');
  vm.loading = true;

  vm.getNews = function() {
    newsService.getList()
      .then(function (list) {
        vm.newsList = list.data;
        vm.loading = false;
        if (list.data.length === 0) {
          throw e;
        }
      })
      .catch(function (e) {
        vm.message = 'Новостей и акций еще нет!'
      })
  };

  vm.getNewsById = function () {
    console.log($stateParams.id);
    console.log('GetNewsById');
    vm.id = $stateParams.id;
    newsService.getById($stateParams.id)
      .then(function (news) {
        vm.news = news.data;
        vm.news.imageUrl = vm.news.imageUrl.replace(/\\/g,'/');
        vm.loading = true;
        console.log(news);
      })
  };

  vm.action = function() {
    angular.element('.modal').addClass('active');
  };
  vm.openModelList = function () {
    angular.element('#profile').show();
  };
  vm.hideModelList = function () {
    angular.element('#profile').hide();
    angular.element('#vin-btn').click();
  };
}];

export default newsController

