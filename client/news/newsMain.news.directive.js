'use strict';

import newsListTemplate from './view/list.news.view.html';

const newsListDirective = [ function () {
   return {
       restrict:'E',
       template: newsListTemplate,
       controller: 'newsController',
       controllerAs: 'vm'
   }
}];

export default newsListDirective