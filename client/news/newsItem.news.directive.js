'use strict';

import newsItemTemplate from './view/item.news.view.html';

const newsItemDirective = [ function () {
    return {
        scope:{
          newsid:'@',
          title:'@',
          meta:'@',
          author:'@',
          content:'@',
          img:'@'
        },
        restrict:'E',
        template: newsItemTemplate
    }
}];

export default newsItemDirective