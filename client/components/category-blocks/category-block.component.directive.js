'use strict';
import categoryBlockTemplate from './view/category-blocks.client.components.view.html'
import './scss/category-block.scss'

const categoryBlockDirective = [function () {

    return {
        scope: {},
        restrict: 'E',
        template: categoryBlockTemplate,
        link: function (scope, element, attr) {
            let block = angular.element('.category-block-item');

            block.on('mouseover', function () {
               $(this).find('p').addClass('active');
            })
            block.on('mouseleave', function () {
                $(this).find('p').removeClass('active');
            })


        }
    }
}]

export default categoryBlockDirective