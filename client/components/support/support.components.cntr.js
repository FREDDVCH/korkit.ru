'use strict';

const SupportController = ['SupportService', '$timeout', '$element', function (SupportService, $timeout, $element) {

  let vm = this;
  vm.name = '';
  vm.telephone = '';
  vm.textarea = '';
  vm.email = '';
  vm.nextStep = false;

  vm.message = 'Ваше сообщение отправляется...';
  vm.loader = false;

  vm.sendSupport = function () {


    var detail = angular.element('.modal');


    if (vm.name.length !== 0 && vm.telephone.match(/\d{11}/)) {

      let form = {
        name: vm.name,
        tel: vm.telephone,
      };

      if (detail.attr('id') && detail.attr('data')) {
        form.part = detail.attr('id');
        form.storage = detail.attr('data');
      }

      vm.loader = true;

      SupportService.sendMail(form)
        .then(function (res) {
          if (res.data.status === 'error') {
            vm.loader = false;
            vm.status = res.data.status;
            vm.message = res.data.message;
            vm.nextStep = true;
            $timeout(function () {
              vm.danger = undefined;
              vm.nextStep = false;
            },2500);
            return;
          }
          vm.name = '';
          vm.telephone = '';
          vm.nextStep = true;
          vm.loader = false;
          vm.message = res.data.message;
          vm.status = res.data.status;
          $timeout(function () {
            vm.danger = undefined;
            vm.nextStep = false;
          },2500);
        });
    } else {
      vm.danger = 'Поля c * обязательны для заполнения'
    }
  }

  vm.sendMail = function () {
    if (vm.name.length !== 0 && vm.telephone.match(/\d{11}/) && vm.email.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/) && vm.textarea.length !== 0){
      vm.danger = undefined;

      let form = {
        name: vm.name,
        tel: vm.telephone,
        message: vm.textarea,
        email: vm.email
      };

      vm.loader = true;

      SupportService.sendMail(form)
        .then(function (res) {
          if (res.data.status === 'error') {
            vm.loader = false;
            vm.message = res.data.message;
            vm.nextStep = true;
            return;
          }
          vm.nextStep = true;
          vm.loader = false;
          vm.message = res.data.message;
        });

    } else {
      vm.danger = 'Поля указынные * обязательны для заполнения'
    }

  }

  function clearForm() {
    vm.name = '';
    vm.telephone = '';
    vm.textarea = '';
    vm.email = '';
  }

}];

export default SupportController;