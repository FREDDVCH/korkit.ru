'use strict';

import supportDirective from './support.components.directive';
import supportService from '../../../services/support/ticked';
import supportController from './support.components.cntr';

const supportModule = angular.module('Support', []);


supportModule.config(['$stateProvider', '$urlRouterProvider',function ($stateProvider, $urlRouterProvider) {

    $stateProvider.state('support',{
        url: '/support',
        data: {
          title: 'Служба поддержки клиентов Korkit.ru'
        },
        template: '<support></support>'
    })

}]);


supportModule.directive('support', supportDirective);
supportModule.service('SupportService', supportService);
supportModule.controller('supportController', supportController);

export default supportModule;

