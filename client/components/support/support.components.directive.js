'use strict';

import supportTemplate from './view/support.component.view.html'
import './scss/support.scss'

const suppDirective = [ function () {
   return{
       restrict: 'E',
       template: supportTemplate,
       controller: 'supportController',
       controllerAs: 'sc'
   }
}];

export default suppDirective;