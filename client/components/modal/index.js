'use strict';

import modalDirective from './main.modal.directive'
import './scss/modal.scss'

const modal = angular.module('Modal',[])

modal.directive('modal',modalDirective);
export default modal;