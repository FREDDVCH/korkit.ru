'use strict';

import modalTemplate from './view/modalTemplate.modal.view.html'

const modalDirective = [function () {
    return {

        scope:{
            isHidden: '=',
            modalHeader:'=',
            modalDescription:'='
        },
        restrict:'E',
        template: modalTemplate,
        link: function (scope, elem, attr) {

            // var vm = this;
            var self = this;
            scope.close = function() {
                console.log('link in running');
                angular.element('.modal').removeClass('active');
                  angular.element('.modal').removeAttr('id');
                  angular.element('.modal').removeAttr('data');
            };

        },
      controller: 'supportController',
      controllerAs: 'sup'
    }
}];

export default modalDirective