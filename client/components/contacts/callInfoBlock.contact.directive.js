'use strict';

import callInfoTemplate from './view/callInfoBlock.contact.view.html'

const callInfoBlock = [function () {
   return {
       scope: {
         tel:'@',
         email:'@',
         adress:'@'
       },
       restrict: 'E',
       template: callInfoTemplate,
       controller: 'mainCoreController',
       controllerAs:'main'
   }
}];

export default callInfoBlock