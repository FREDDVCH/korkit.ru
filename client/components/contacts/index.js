'use strict';
import contactDirective from  './callInfoBlock.contact.directive'
import './scss/contacts.scss'

const contactsModule = angular.module('Contacts',[]);

contactsModule.directive('contact', contactDirective);

export default contactsModule;