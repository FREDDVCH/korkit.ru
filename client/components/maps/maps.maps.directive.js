'use strict'

import mapsTemplate from './view/template-maps.maps.view.html'


const mapsDirective = function () {
    return {
        restrict: 'E',
        template: mapsTemplate,
        controller:"MapsCntr",
        controllerAs:'ym'
    }
}

export default mapsDirective;
