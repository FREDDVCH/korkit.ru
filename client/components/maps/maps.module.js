'use strict'
// import './../../../node_modules/angular-ymaps/angular-ymaps'
import './../../../node_modules/angular-ymaps/angular-ymaps'
import mapsDirective from './maps.maps.directive'
import mapsController from "../info-pages/ymapsCntr";

const maps = angular.module('Maps',['ymaps']);

maps.directive('maps',mapsDirective);
maps.controller('MapsCntr',mapsController);

export default maps;