'use strict';

const YmapCntr = ['$scope', function($scope) {
    //создаем массив координат и данных для меток и балунов
    $scope.markers = [
        {coordinates:[52.53722157172731,85.21898499999998], properties: {balloonContent: 'г. Бийск, Алтайский край: ул. Советская, 60'}},
        {coordinates:[52.5312885717121,85.18431000000004], properties: {balloonContent: 'г. Бийск, Алтайский край: ул. Мерлина, 11'}, options: {preset: 'islands#icon', iconColor: '#a5260a'}},
        {coordinates:[55.05135506965954,82.93891699999989], properties: {balloonContent: 'г. Новосибирск: ул Николая Островского, 120 (на пересечении ул Ипподромской  и ул. Писарева, вход со стороны ул.Писарева. '}}
    ];
    //настройки положения карты
    $scope.map = {
        center: [55.05128380915124,82.93898999098876], zoom: 17
    };
}];

export default YmapCntr
