'use strict'

import returnConditionsTemplate from './view/return-conditions.info.view.html'

const returnConditionsDirective = [function () {
    return {
        restrict: 'E',
        template: returnConditionsTemplate
    }
}]

export  default returnConditionsDirective