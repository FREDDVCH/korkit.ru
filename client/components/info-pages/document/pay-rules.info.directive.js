'use strict';

import payRulesTemplate from './view/pay-rules.info.view.html'

const payRulesDirective = [function () {
    return {
        restrict: 'E',
        template: payRulesTemplate
    }
}];

export  default payRulesDirective