'use strict';

const confidenceController = ['$rootScope','$state','$stateParams','$element','PageService', function ($rootScope, $state, $stateParams, $element, PageService) {

  var vm = this;
  vm.list = [];

  let params = $stateParams.page;

  onInit();

  vm.getPage = function () {
    PageService.getById(params)
      .then(function (res) {
        vm.page = res.data;
        console.log(vm.page, 'page is')
      })
  };

  function onInit() {
    PageService.getList()
      .then(function (res) {
        vm.list = res.data;
        console.log(vm.list, 'list is');
      })
  };

}];

export default confidenceController
