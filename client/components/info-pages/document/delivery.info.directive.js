'use strict'

import deliveryTemplate from './view/delivery.info.view.html'

const deliverDirective = [function () {
    return {
        restrict: 'E',
        template: deliveryTemplate
    }
}]

export  default deliverDirective