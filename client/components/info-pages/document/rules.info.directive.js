'use strict'

import rulesTemplate from './view/rules.info.view.html'

const rulesDirective = [function () {
    return {
        restrict: 'E',
        template: rulesTemplate
    }
}]

export  default rulesDirective