'use strict';

import confidenceTemplate from './view/confidence.info.view.html'

const confidenceDirective = [function () {
  return {
    restrict: 'E',
    template: confidenceTemplate,
    controller: 'pageController',
    controllerAs: 'pc'
  }
}];

export  default confidenceDirective