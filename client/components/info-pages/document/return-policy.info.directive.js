'use strict'

import returnPolicyTemplate from './view/return-policy.info.view.html'

const returnPolicyDirective = [function () {
    return {
        restrict: 'E',
        template: returnPolicyTemplate
    }
}]

export  default returnPolicyDirective