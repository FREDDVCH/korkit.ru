'use strict'

import aboutTemplate from './view/about.info.view.html'

const aboutDirective = [function () {
    return {
        restrict: 'E',
        template: aboutTemplate
    }
}]

export  default aboutDirective