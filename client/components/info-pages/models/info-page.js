var mongoose = require('mongoose');

var infoPageSchema = new mongoose.Schema({
    title: String,
    content: String,
    author: { type: Schema.Types.ObjectId, ref:'User' },
    created: {type: Date, defauly: Date.now},
});

module.exports = mongoose.model('InfoPage', infoPageSchema);