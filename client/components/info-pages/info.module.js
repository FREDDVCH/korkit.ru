'use strict'
import './css/info.css'
import aboutDirective from './about/about.info.directive'
import rulesDirective from './document/rules.info.directive'
import returnPolDirective from './document/return-policy.info.directive'
import returnCondDirective from './document/return-conditions.info.directive'
import deliveryDirective from './document/delivery.info.directive'
import payRulesDirective from './document/pay-rules.info.directive'
import shopListDirective from './shoplist/list.info.shop.directive'
import confidenceDirective from './document/confidence-policy'
import confidenceController from './document/page.info.cntr'
import pageService from './../../../services/page/pageService'
import './../contacts'
import './scss/info-page.scss'

import mapsController from './ymapsCntr'

const infoModule = angular.module('InfoModule',['ymaps','Contacts']);

infoModule.config(['$stateProvider','$urlRouterProvider', function ($stateProvider,$urlRouterProvider) {
    $stateProvider.state('about',{
        url:'/about',
        data : {
            title: 'О магазине автозапчастей KorKit.ru'
        },
        template: '<about></about>'
    })
    $stateProvider.state('rules',{
        url:'/rules',
        data : {
            title: 'Правила интернет-магазина KorKit.ru'
        },
        template: '<rules></rules>'
    })
    $stateProvider.state('return-pol',{
        url:'/return-policy',
        data : {
            title: 'Политика возврата автозапчастей KorKit.ru'
        },
        template: '<return-policy></return-policy>'
    })
    $stateProvider.state('return-cond',{
        url:'/return-conditions',
        data : {
            title: 'Условия возврата автозапчастей KorKit.ru'
        },
        template: '<return-conditions></return-conditions>'
    })
    $stateProvider.state('delivery',{
        url:'/delivery',
        data : {
            title: 'Доставка автозапчастей KorKit.ru'
        },
        template: '<delivery></delivery>'
    })
    $stateProvider.state('shop',{
        url:'/shoplist',
        data : {
            title: 'Магазины автозапчастей KorKit.ru'
        },
        template: '<shop-list></shop-list>'
    })
    $stateProvider.state('pay-rules',{
        url:'/pay',
        data : {
            title: 'Как оплатить заказ KorKit.ru'
        },
        template: '<pay-rules></pay-rules>'
    })
    $stateProvider.state('page',{
        url:'/page/:page',
        data : {
            title: 'Информация покупателям'
        },
        template: '<confidence></confidence>'
    })

}])

infoModule.controller('pageController',confidenceController);
infoModule.service('PageService', pageService);
infoModule.directive('shopList',shopListDirective);
infoModule.directive('about', aboutDirective);
infoModule.directive('payRules', payRulesDirective);
infoModule.directive('rules', rulesDirective);
infoModule.directive('delivery', deliveryDirective);
infoModule.directive('returnPolicy', returnPolDirective);
infoModule.directive('returnConditions', returnCondDirective);
infoModule.directive('confidence', confidenceDirective);

export default infoModule;