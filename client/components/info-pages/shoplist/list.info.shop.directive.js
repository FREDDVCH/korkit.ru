'use strict';

import shopList from './view/list.info.shop.directive.html'

const shopListDirective = [function () {

    return {
        restrict:'E',
        template: shopList
    }

}];

export default shopListDirective