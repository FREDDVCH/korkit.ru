'use strict'

import pageTitle from './view/pageTitle.core.directive.view.html'

const titleDirective = ['$transitions','$rootScope', '$injector', function ($transitions, $injector, $rootScope) {
        return {
            restrict:'E',
            scope: {},
            link: function(scope) {
                $transitions.onStart({}, function($transition$) {
                    scope.pageTitle = $transition$.$to().data.title;
                })
            },
            template: pageTitle
        };
    }];

export default titleDirective