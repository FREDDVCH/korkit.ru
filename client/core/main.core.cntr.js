'use strict'

const mainCore = ['$rootScope','$state','$element', 'PageService', function ($rootScope, $state, $element, PageService) {

    var vm = this;
    vm.list = [];

    vm.action = function(data = '') {
        if (data !== '') {
          angular.element('.modal').attr('id', data.name);
          angular.element('.modal').attr('data', data.storage);
        }
        angular.element('.modal').addClass('active');
    };
    vm.openModelList = function () {
        angular.element('#profile').show();
    };
    vm.hideModelList = function () {
        angular.element('#profile').hide();
        angular.element('#vin-btn').click();
    };

    onInit();

    function onInit() {
      PageService.getList()
        .then(function (res) {
            vm.list = res.data;
        })
    }
}];

export default mainCore

