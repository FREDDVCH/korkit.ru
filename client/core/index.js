'use strict';
// import footerPart from '../parts/'
// import angular from 'angular';
// import 'angular-ui-router';
import categoryBlocks from '../components/category-blocks/view/category-blocks.client.components.view.html'
import mainCoreCntrl from './main.core.cntr'
import titleDirective from './title.core.directive'
import './css/style.css'
import './scss/base.scss'
import './scss/layout.scss'
import './scss/module.scss'
import './scss/state.scss'
import './scss/typography.scss'
import categoryBlockDirective from "../components/category-blocks/category-block.component.directive"

const coreController = angular.module('App', ['ui.bootstrap','appParts','ui.router','authModule','AuthService','SearchPartService','InfoModule','Maps','CatalogsModule','ymaps','Modal','Autocatalog','Support','NewsModule', 'userModule']);

coreController.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('home',{
        abstract: true,
        url: '',
        data : {
            title: 'Интернет магазин автозапчастей KorKit.ru'
        },
        controller: mainCoreCntrl,
        template: '<ui-view>'
    });
    $stateProvider.state('home.main',{
        url: '/',
        data : {
            title: 'Интернет магазин автозапчастей KorKit.ru'
        },
        template: '<category-blocks></category-blocks>'
    });

}]);


coreController.run(['$transitions','$rootScope', '$injector', ($transitions, $injector, $rootScope,) => {
    $transitions.onBefore({}, function($transition$) {
        $rootScope.pageTitle = $transition$.$to().data.title;
    })
}]);

coreController.controller('mainCoreController', mainCoreCntrl);
coreController.directive('categoryBlocks', categoryBlockDirective);
coreController.directive('title',titleDirective);


export default coreController

