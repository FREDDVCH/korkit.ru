'use strict';

import sidebar from '../sidebar/view/sidebar.parts.view.html'
import sidebarController from './sidebar.parts.cntr'

const partSidebar = function () {
    return {
        restrict:'E',
        template: sidebar,
        controller: sidebarController
    }
};

export default partSidebar
