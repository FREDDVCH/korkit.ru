'use strict';

import partHeader from './header/header.parts.directive';
import partSidebar from './sidebar/sidebar.parts.directives';
import partFooter from './footer/footer.parts.directives';
import partModelListDirective from './header/modelList.parts.directive';
import './sidebar/css/sidebar.css'
import './header/scss/header.scss'
import './footer/scss/footer.scss'

const appParts = angular.module('appParts',[]);

appParts.directive('header', partHeader);
appParts.directive('modelList', partModelListDirective);
appParts.directive('footer', partFooter);
appParts.directive('sidebar', partSidebar);

export default appParts
