'use strict';
import footer from '../footer/view/footer.parts.view.html'

const partFooter = function () {
    return {
        restrict:'E',
        template: footer,
        controller: 'mainCoreController',
        controllerAs: 'vm'
    }
}

export default partFooter
