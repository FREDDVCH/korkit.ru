'use strict';
import header from '../header/view/header.parts.view.html'

const partHeader = function () {
    return {
        restrict:'E',
        template: header,
        controller:'AuthMainController',
        controllerAs:'am',
    }
};

export default partHeader
