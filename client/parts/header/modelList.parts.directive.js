'use strict';

import partModelList from './view/modelList.parts.view.html';

const  partModelDirective = [function () {

   return {
       restrict: 'E',
       template: partModelList,
   }
}];

export default partModelDirective