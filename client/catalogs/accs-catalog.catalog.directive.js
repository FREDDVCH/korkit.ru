'use strict';

import htmpAccessoriesTemplate from './view/accs-catalog.catalog.view.html'

const accessoriesDirective = [function () {
    return {
        restrict:'E',
        template: htmpAccessoriesTemplate,
        controller: 'itemsDetailController',
        controllerAs: 'vm'
    }
}];

export default accessoriesDirective