'use strict';

import accsItemTEmplate from './view/accs-item.catalog.view.html'

const accsItemDirective = [function () {
    return{
        restrict: 'E',
        scope:{
            description:'=',
            price:'=',
            source:'=',
            status:'='
        },
        template: accsItemTEmplate
    }
}]

export default accsItemDirective