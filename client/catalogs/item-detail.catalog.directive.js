'use strict';

import itemDetailTemplate from './view/item-detail.catalog.view.html'

const itemDetailPage = [function () {
   return {
       restrict: 'E',
       template: itemDetailTemplate,
       controller: 'itemsDetailController',
       controllerAs:'vm'
   }
}];

export default itemDetailPage;