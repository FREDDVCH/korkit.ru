'use strict';

import catalogTemplate from './view/catalog-category.catalog.view.html'

const catalogDirective = [function () {
    return{
        restrict: 'E',
        template: catalogTemplate,
        controller: 'itemsDetailController',
        controllerAs:'vm'
    }
}];

export default catalogDirective