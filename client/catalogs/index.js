'use strict';

import itemsDetailCntr from './itemsDetailCntr.catalog.cntr'
import accessoriesDirective from './accs-catalog.catalog.directive'
import accessoriesItemDirective from './accs-item.catalog.directive'
import itemDetailPage from './item-detail.catalog.directive'
import oilDirective from './oil-catalog.catalog.directive'
import battaryDirective from './battary-catalog.catalog.directive'
import catalogDirective from './mainCatalog.catalog.directive'
import './css/catalogs.css'
import './scss/catalogs.scss';
const catalogsModule = angular.module('CatalogsModule',[]);

catalogsModule.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('catalog',{
            abstract:true,
            url:'/catalog',
            template: '<ui-view></ui-view>'
        })
        .state('catalog.accessories',{
            url:'/accessories',
            data:{
              title:'Аксессуары для вашего авто'
            },
            template: '<accessories></accessories>'
        })
        .state('catalog.oil',{
            url:'/oil',
            data:{
                title:'Масло для вашего авто'
            },
            template: '<oil></oil>'
        })
        .state('catalog.battary',{
            url:'/battary',
            data:{
                title:'Масло для вашего авто'
            },
            template: '<battary></battary>'
        })
        .state('catalog.category', {
            url:'/:catalog',
            data: {
                title: 'Каталог товаров для вашего авто'
            },
            template: '<catalog></catalog>'
        })
        .state('catalog.item', {
            url:'/:catalog/:item',
            reloadOnSearch: false,
            data: {
                title: 'Товары для вашего авто'
            },
            template: '<item-detail></item-detail>'
        })
}]);


catalogsModule.controller('itemsDetailController', itemsDetailCntr);
catalogsModule.directive('catalog',catalogDirective);
catalogsModule.directive('itemDetail',itemDetailPage);
catalogsModule.directive('accessories', accessoriesDirective);
catalogsModule.directive('accessoriesItem', accessoriesItemDirective);
catalogsModule.directive('oil', oilDirective);
catalogsModule.directive('battary', battaryDirective);

export default catalogsModule