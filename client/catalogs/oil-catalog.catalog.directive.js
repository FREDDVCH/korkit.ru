'use strict';

import htmpOilTemplate from './view/oil-catalog.catalog.view.html'

const oilDirective = [function () {
    return {
        restrict:'E',
        template: htmpOilTemplate,
        controller: 'itemsDetailController',
        controllerAs: 'vm'
    }
}];

export default oilDirective