'use strict';

const itemsDetailCntr = ['$http','$stateParams','$transitions','$location',function ($http, $stateParams, $transitions,$location) {
    let vm = this,
        id = $stateParams.item;
    // var cat = $stateParams.catalog;

    vm.getAllCategoryItem = function () {

        var str = $location.path(),
            match = str.match(/(\w*)/g),
            result = [],
            cat = $stateParams.catalog ? $stateParams.catalog : match[3] ;

        if(cat == 'oil'){
            this.title = 'Каталог автомобильных масел';
            this.items = findByCategory(cat);
        } else if(cat == 'accs'){
            this.title = 'Каталог авто аксессуаров';
            console.log('send 3 catalog');
            this.items = findByCategory(cat);
        } else if(cat == 'battary'){
            this.title = 'Каталог аккумуляторных батарей';
            console.log('send 2 catalog');
            this.items = findByCategory(cat);
        } else if(cat == 'glass'){
            this.title = 'Каталог щеток стеклоочистителя';
            console.log('send 1 catalog');
            this.items = findByCategory(cat);
        }

        function findByCategory(cat) {
            var result = [],
                arrayAll = [
                {
                    name:'Castrol',
                    brand:'name',
                    price:'1580',
                    image:'http://www.supercheapauto.com.au/Product/Nulon-Full-Synthetic-Long-Life-Engine-Oil-5W-30-6-Litre/343508',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 11,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'MAN',
                    price:'1590',
                    image:'http://media.supercheapauto.com.au/sca/images/zooms/215340-zoom.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 12,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'Some',
                    price:'2580',
                    image:'http://www.supercheapauto.co.nz/Product/Castrol-GTX-Engine-Oil-20W-50-4-Litre/102067',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 13,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'Heavy',
                    price:'1080',
                    image:'http://media.supercheapauto.com.au/sca/images/zooms/215340-zoom.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 14,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'CARd',
                    price:'1500',
                    image:'http://media.supercheapauto.com.au/sca/images/zooms/215340-zoom.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 15,
                    status:false,
                    cat:'oil'
                },
                {
                    name:'Castrol',
                    brand:'Audi',
                    price:'1980',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 1,
                    status:false,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'BMW',
                    price:'1280',
                    image:'http://www.taoportal.ru/upload/multimedia/medium/%D0%90%D0%BA%D1%81%D0%B5%D1%81%D1%81%D1%83%D0%B0%D1%80%D1%8B%20%D0%B4%D0%BB%D1%8F%20%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8F%204.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 2,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Перчатки автолюбительские',
                    brand:'Toyota',
                    price:'1590',
                    image:'https://mi3.rightinthebox.com/images/190x190/201703/xohjcp1489373123616.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 3,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'Subaru',
                    price:'1180',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 4,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'Kia',
                    price:'1586',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 5,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'Noname',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 6,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Lada',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 7,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Bentley',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 8,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Ford',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 9,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Lancer',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 10,
                    status:true,
                    cat:'glass'
                }
            ];

            console.log('Внутри поиска по категории');

            for (let item of arrayAll) {
                if(item.cat == cat) {
                    console.log(item);
                    result.push(item);
                }
            }
            return result;
        }

    };

    vm.getOneItem = function () {
        let id = $stateParams.item;
        console.log(id);

        this.item = findById(id);

        function findById(id) {
            var arrayAll = [
                {
                    name:'Castrol',
                    brand:'name',
                    price:'1580',
                    image:'http://www.supercheapauto.com.au/Product/Nulon-Full-Synthetic-Long-Life-Engine-Oil-5W-30-6-Litre/343508',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 11,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'MAN',
                    price:'1590',
                    image:'http://media.supercheapauto.com.au/sca/images/zooms/215340-zoom.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 12,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'Some',
                    price:'2580',
                    image:'http://www.supercheapauto.co.nz/Product/Castrol-GTX-Engine-Oil-20W-50-4-Litre/102067',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 13,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'Heavy',
                    price:'1080',
                    image:'http://media.supercheapauto.com.au/sca/images/zooms/215340-zoom.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 14,
                    status:false,
                    cat: 'oil'
                },
                {
                    name:'Castrol',
                    brand:'CARd',
                    price:'1500',
                    image:'http://media.supercheapauto.com.au/sca/images/zooms/215340-zoom.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 15,
                    status:false,
                    cat:'oil'
                },
                {
                    name:'Castrol',
                    brand:'Audi',
                    price:'1980',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 1,
                    status:false,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'BMW',
                    price:'1280',
                    image:'http://www.taoportal.ru/upload/multimedia/medium/%D0%90%D0%BA%D1%81%D0%B5%D1%81%D1%81%D1%83%D0%B0%D1%80%D1%8B%20%D0%B4%D0%BB%D1%8F%20%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8F%204.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 2,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Перчатки автолюбительские',
                    brand:'Toyota',
                    price:'1590',
                    image:'https://mi3.rightinthebox.com/images/190x190/201703/xohjcp1489373123616.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 3,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'Subaru',
                    price:'1180',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 4,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'Kia',
                    price:'1586',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 5,
                    status:true,
                    cat: 'accs'
                },
                {
                    name:'Castrol',
                    brand:'Noname',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 6,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Lada',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 7,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Bentley',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 8,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Ford',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 9,
                    status:true,
                    cat:'battary'
                },
                {
                    name:'Castrol',
                    brand:'Lancer',
                    price:'1580',
                    image:'http://www.mobilluck.com.ua/files/cat_icons/car_adapters_1.jpg',
                    descr:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet consequatur cupiditate distinctio hic minima perferendis qui repudiandae. Ab asperiores consequuntur dignissimos, dolore est exercitationem hic minima sequi! Eos, itaque repudiandae.',
                    id: 10,
                    status:true,
                    cat:'glass'
                }
            ];

            console.log('Внутри поиска');

            let result = {message: 'Такой товар не найден!'};

            for (let item of arrayAll) {
                if(item.id == id) {
                    console.log(item);
                    result = item;
                    return result;
                }
            }

        }

    }

}];

export default itemsDetailCntr;