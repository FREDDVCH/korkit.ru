'use strict';

import battaryItemTEmplate from './view/battary-catalog.catalog.view.html'

const battaryItemDirective = [function () {
    return{
        restrict: 'E',
        template: battaryItemTEmplate,
        controller: 'itemsDetailController',
        controllerAs:'vm'
    }
}];

export default battaryItemDirective