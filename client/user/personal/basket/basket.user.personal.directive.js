'use strict';

import basketTemplate from './basket.user.personal.view.html'

const basket = function () {
  return {
    restrict: 'E',
    template: basketTemplate,
    controller: 'OrderController',
    controllerAs: 'oc'
  }
};

export default basket;