'use string';

import garageListTemplate from './garage-list.user.personal.view.html'

const garage = ['Auth','$transitions',function (Auth,$transitions) {

  return {
    restrict:'E',
    template: garageListTemplate,
    controller: 'GaragePersonalController',
    controllerAs: 'vm'
  }

}];

export default garage