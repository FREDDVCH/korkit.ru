'use strict';

const  garagePersonalCntr = ['$rootScope','$scope','Auth','User','carInfo', function ($rootScope, $scope, Auth, User, carInfo) {

  let vm = this;
  vm.isShowCar = false;
  vm.isEditMode = false;

  vm.getCars = function() {

    Auth.getUser().then(function (user) {
      User.getUser(user.data._id)
        .then(function (user) {
          $rootScope.user = user.data[0];
          vm.garage = $rootScope.user.garage;
        },function (err) {
          console.log(err);
        });
    });

    carInfo.getYearList().then(function (years) {
      vm.years = years.data;
    });
  };

  vm.editMode = function () {
    vm.isEditMode = true;

  };

  vm.getBrand = function () {
    vm.isShowCar = false;
    carInfo.getBrands(vm.year).then(function (brands) {
      vm.brands = brands.data;
    });
  };

  vm.getModel = function () {
    vm.isShowCar = false;

    let req = {
      year: vm.year,
      brand: vm.brand
    };
    carInfo.getModels(req).then(function (models) {
      vm.models = models.data;
    });

  };

  vm.getSpec = function () {
    vm.isShowCar = false;

    let req = {
      year: vm.year,
      model: vm.model
    };
    carInfo.getSpec(req).then(function (specs) {
      vm.specs = specs.data;
      console.log(vm.spec);
    })
  };

  vm.createCar = function () {

    let car = {
      brand: vm.brand.charAt(0).toUpperCase(),
      model: vm.model,
      spec: JSON.parse(vm.spec),
      year: vm.year,
      vin: vm.vin
    };
    console.log(car);

    $rootScope.user.garage.push(car);

    Auth.putUser($rootScope.user).then(function (user) {
      vm.message = {message: 'обновление успешно'}
    });

    vm.garage = $rootScope.user.garage;
    vm.specs = [];
    vm.isShowCar = false;
    vm.isEditMode = false;
  };

  vm.cancelEdit = function () {
    vm.isEditMode = false;
  };

  vm.showCar = function () {
    vm.specification = JSON.parse(vm.spec);
    vm.isShowCar = true;
    // $scope.$apply();
  };

}];

export default garagePersonalCntr