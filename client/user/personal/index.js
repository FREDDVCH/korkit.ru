'use strict';

import userMetaDirective from './userMeta.user.personal.directive'
import guestSuccessCreated from './orders/success-created-order.user.personal.view.html'
import personalPage from './view/personal-page.user.client.view.html'
import personalEditPage from './view/personal-edit.user.personal.client.view.html'
import personalGaragePage from './garage/garage.user.personal.view.html'
import guestBasketPage from './../guest/guest.user.guest.view.html'
import orderListDirective from './orders.user.personal.directive'
import personalGarageDirectrive from './garage/garage.user.personal.directive'
import orderCntr from './orders.user.personal.cntr'
import garageCntr from './garage/garage.user.personal.cntr'
import orderDetail from './orders/order-detail/order-detail.user.personal.view.html'
import OrderService from './../../../services/personal/orderService'
import LocalStorageServise from './../../../services/local/localStorageService'
import carInfoService from './../../../services/garage/garageInfo'
import basketDirective from './basket/basket.user.personal.directive'
import guestCreateOrder from "./orders/create-order.user.personal.view.html"

const userModule = angular.module('userModule',['ui.router']);

userModule.config(['$httpProvider','$stateProvider','$urlRouterProvider','$locationProvider', function($httpProvider,$stateProvider) {

  $stateProvider.state('personal', {
    abstract: true,
    url: '/personal',
    template: '<ui-view>'
  });

  $stateProvider.state('personal.page', {
    url: '/page/:userId',
    data: {
      title: 'Личный кабинет'
    },
    template: personalPage
  });

  $stateProvider.state('personal.create', {
    url: '/page/:userId/create-order',
    data: {
      title: 'Оформление заказа'
    },
    controller: 'OrderController',
    controllerAs: 'oc',
    template: guestCreateOrder
  });


  $stateProvider.state('personal.order', {
    url: '/order/:id',
    data: {
      title: 'Детали заказа'
    },
    template: orderDetail
  });

  $stateProvider.state('personal.edit',{
    url: '/edit',
    data: {
      title: 'Редактирование пользователя'
    },
    template: personalEditPage
  });
  $stateProvider.state('personal.garage',{
    url: '/garage',
    data: {
      title: 'Гараж пользователя'
    },
    template: personalGaragePage
  });
  $stateProvider.state('guest',{
    abstract: true,
    url: '/guest',
    template: '<ui-view>'
  });
  $stateProvider.state('guest.basket',{
    url: '/basket',
    data: {
      title: 'Добро пожаловать'
    },
    template: guestBasketPage
  });

  $stateProvider.state('guest.create',{
    url: '/create-order',
    data: {
      title: 'Создание заказа'
    },
    controller: 'OrderController',
    controllerAs: 'oc',
    template: guestCreateOrder
  })

  $stateProvider.state('guest.success',{
    url: '/success-created',
    data: {
      title: 'Успешно отрпавлен!'
    },
    controller: 'OrderController',
    controllerAs: 'oc',
    template: guestSuccessCreated
  })
}]);

userModule.controller('OrderController', orderCntr);
userModule.controller('GaragePersonalController', garageCntr);
userModule.directive('garage', personalGarageDirectrive);
userModule.directive('orderList', orderListDirective);
userModule.directive('basket', basketDirective);
userModule.factory('carInfo', carInfoService);
userModule.factory('orderService', OrderService);
userModule.factory('localStorageService', LocalStorageServise);
export default userModule