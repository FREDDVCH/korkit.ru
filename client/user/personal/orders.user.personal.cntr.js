'use strict';

const  ordersController = ['$rootScope','$uibModal', '$state','$timeout','$stateParams', 'orderService','Auth','localStorageService', 'SupportService', 'SearchParts', 'PageService',function ($rootScope, $uibModal, $state, $timeout, $stateParams, orderService, Auth, localStorageService, SupportService, SearchParts, PageService) {

  let vm = this;
  let id = $stateParams.id,
      userId = $stateParams.userId;

  vm.basket = [];
  vm.access = false;
  vm.userId = userId;
  let order,
    date = new Date(Date.now());

  vm.closeAttention = function(event) {
    console.log(event.target);
    let par = $(event.target).parent();
    console.log(par,'parent');
    let element = par.parent();
    console.log(element, 'element');
    element.removeClass('show-message');
    $timeout(function () {
      element.addClass('hide');
    },500)
  };

  vm.checkBasket = function () {
    let order =  JSON.stringify(vm.basket.filter( (item) => !item.hasOwnProperty('supplier') ));
    let element = $('#message-box');
    SearchParts.checkList(order)
      .then( (res) => {
        if (res.data.status === 'warning') {
          let result = res.data.result;
          vm.basket.forEach((el, index) => {
            result.forEach(newEl => {
              if (el.brand === newEl.brand && el.vin === newEl.vin && el.orderStorage.name === newEl.orderStorage.name) {
                vm.basket[index] = newEl;
              }
            })
          });
          vm.basket = vm.basket.filter(el => el.orderCount !== 0);
          localStorageService.setList(vm.basket);
          vm.message = res.data.message;
          element.addClass('show-message');
          element.removeClass('hide');
        } else if (res.data.status === 'error') {
          vm.message =  res.data.message;
          vm.status =  res.data.status;
          element.addClass('show-message');
          element.removeClass('hide');
        } else {
          vm.message = res.data.message;
          vm.status = res.data.status;
          element.addClass('show-message');
          element.removeClass('hide');
        }
      });
  };

  vm.changeAccess = function () {
    vm.access = !vm.access;
  };

  localStorageService.getItems().then(items => vm.basket = items);

  vm.sum = function () {
    let sum = {};
    sum.sum = 0;
    sum.count = 0;
    vm.basket.forEach(item => {
      sum.sum += item.orderStorage.price*item.orderCount;
      sum.count += item.orderCount;
    });

    return sum;
  };

  vm.createGuestOrder = function () {

    vm.open();

    localStorageService.getItems()
      .then(
        items => {
          vm.basket = items;
          order = {
            name: vm.name,
            number: vm.storage[0].toUpperCase() + date.getDay() + date.getMonth() + date.getFullYear() + '_' + vm.sum().count +  date.getMilliseconds() + 'GU',
            defaultStorage: vm.storage,
            phone: vm.tel,
            items: vm.basket,
            orderLenght: vm.sum().count,
            cost:  vm.sum().sum,
            guest: true
          };
          if (vm.email) {
            order.email = vm.email;
          }
          console.log(order,'передача на сервер');

          orderService.checkNumber({number: vm.tel})
            .then(function (msg) {
              vm.msg = msg.data.message;
              console.log(vm.msg);
              if (msg.status === 'success') {
                vm.verify = true;
                orderService.createOrder(order).then(order => order.data);
                vm.clearBasket();
              }

            }, function (err) {
              console.log(err);
            });
        });
  };

  vm.createOrder = function () {
    console.log($rootScope.user['defaultStorage'], 'RootScope');
    localStorageService.getItems()
      .then(
        items => {
          vm.basket = items;
          let order = {
            user: userId,
            number: $rootScope.user['defaultStorage'][0].toUpperCase() + date.getDay() + date.getMonth() + date.getFullYear() + '_' + vm.sum().count + date.getMilliseconds(),
            items: vm.basket,
            defaultStorage: $rootScope.user['defaultStorage'],
            orderLenght: vm.sum().count,
            cost:  vm.sum().sum,
            guest: false
          };
          orderService.createOrder(order).then(order => {
            vm.clearBasket();
            $timeout(function () {
              $state.go('guest.success');
            },1500);
          });
        });
  };

  vm.getOrder = function () {
    orderService.getOrder(id).then((order) => {
      vm.order = order.data[0];
      console.log(vm.order);
    });
  };

  vm.getOrders = function () {
    orderService.getOrders(userId, 0, 10).then((orders) => vm.orders = orders.data);
  };

  vm.addItemToBasket = function (item) {
    vm.basket.push(item);
    localStorageService.setItem(vm.basket);
  };

  vm.removeItemFromBasket = function (index) {
    vm.basket = vm.basket.filter((item,$index) =>  $index !== index);
    console.log(vm.basket);
    localStorageService.setList(vm.basket);
  };

  vm.clearBasket = function () {
    vm.basket = [];
    localStorageService.clearLocalStorage();
  };

  vm.onSubmit = function () {

    let body = {
      name: vm.order.user.firstName,
      body: vm.body,
      clientId: vm.order.user._id,
      orderId: vm.order._id
    };

    console.log(body);

    SupportService.postTicked(body)
      .then(function (status) {
        vm.body = '';
        console.log(status);
        return status;
      }).then(function (status) {
        SupportService.getListByOrderId(id)
          .then(function (ticked) {

            vm.tickeds = ticked.data;
            console.log(ticked, 'consol');
          });
    })
  };

  SupportService.getListByOrderId(id)
    .then(function (ticked) {
      vm.tickeds = ticked.data;
    });

  onInit();

  function onInit() {
    $( document ).ready(function () {
      vm.checkBasket()
    });

    PageService.getList()
      .then(function (res) {
        vm.list = res.data;
        console.log(vm.list, 'list is');
      })
  }


  vm.open = function (size, parentSelector) {
    var parentElem = parentSelector ?
      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
    var modalInstance = $uibModal.open({
      animation: vm.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'myModalContent.html',
      controller: 'ModalController',
      controllerAs: 'mc',
      size: size,
      appendTo: parentElem,
      resolve: {
        items: function () {
          return vm.msg;
        }
      }
    });

    modalInstance.result.then(function (success) {
      if (success) {
        orderService.createOrder(order).then(order => order.data);
        vm.clearBasket();
        $timeout(function () {
          $state.go('guest.success');
        },1500);
      } else {
        vm.msg = 'Ошибка сервера, попробуйте позднее!';
      }
    }, function () {
      vm.msg = 'Ошибка сервера, попробуйте позднее!';
    });

  };


}];

export default ordersController