'use strict';

import userMetaTemplate from './view/userMeta.user.personal.view.html'

const userMeta = ['Auth','$transitions',function (Auth,$transitions) {

    return {
        restrict:'E',
        template: userMetaTemplate,
        link: function () {
            Auth.getUser()
                ,then(function (data) {
                console.log('from directive is ');
                $transitions.onStart({}, function ($transition$) {
                    scope.user = data.data;
                });
                    console.log('from directive is ' + scope.user.username)
            })

        }
    }

}];

export default userMeta

