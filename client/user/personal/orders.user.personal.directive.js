'use string';

import ordersTemplate from './view/orders.user.personal.client.view.html'

const orders = ['Auth','$transitions',function (Auth,$transitions) {

  return {
    restrict:'E',
    template: ordersTemplate,
    controller: 'OrderController',
    controllerAs: 'vm'
  }

}];

export default orders