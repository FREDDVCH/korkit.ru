'use strict';

import createOrderGuestTemplate from './create-order.user.personal.view.html'

const createOrderGuest = function () {
  return {
    restrict: 'E',
    template: createOrderGuestTemplate,
    controller: 'OrderController',
    controllerAs: 'oc'
  }
};

export default createOrderGuest;