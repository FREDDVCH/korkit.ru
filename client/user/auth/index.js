'use strict';

import loginPage from './view/login.user.client.view.html'
import signPage from './view/sign-in.user.client.view.html'
import recoveryPage from './view/recovery.user.client.view.html'
import AuthMain from './authMain.user.client.cntr'
import userMainController from './userMain.cntr'

// import './auth.module';
import './css/user.css';
import userMetaDirective from './../personal/userMeta.user.personal.directive'
import modalController from './../auth/modal.user.client.cntr'
import modalWinComponent from './../auth/modal.user.client.component'

const authModule = angular.module('authModule',['ui.router','AuthService']);

authModule.config(['$httpProvider','$stateProvider','$urlRouterProvider','$locationProvider',function($httpProvider,$stateProvider, $urlRouterProvider, $locationProvider,) {

    $httpProvider.interceptors.push('AuthInterceptor');

    $urlRouterProvider.otherwise('/');
    $stateProvider.state('auth', {
        abstract: true,
        url: '/auth',
        template: '<ui-view>'
    });
    $stateProvider.state('auth.login',{
        url: '/login',
        controller: 'AuthMainController',
        controllerAs:'vm',
        data: {
            title: 'Войти в свой кабинет'
        },
        template: loginPage
    });
    $stateProvider.state('auth.sign-in',{
        url: '/sign-in',
        controller: 'AuthMainController',
        controllerAs:'vm',
        data: {
            title: 'Регистрация на сайте KorKit.ru'
        },
        template: signPage
    });
    $stateProvider.state('auth.recovery',{
        url: '/recovery',
        controller: 'AuthMainController',
        controllerAs:'vm',
        data: {
            title: 'Восстановление пароля на сайте KorKit.ru'
        },
        template: recoveryPage
    });

   // $locationProvider.html5Mode(true);

}]);

authModule.controller('AuthMainController', AuthMain);
authModule.controller('UserMainController', userMainController);
authModule.controller('ModalController', modalController);
authModule.component('modalComponent', modalWinComponent);
authModule.directive('userMeta', userMetaDirective);

export default authModule