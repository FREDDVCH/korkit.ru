'use strict';

const userMainController = ['$uibModal', 'User','Auth', '$timeout','$state','$location','$transitions', '$interval', 'PageService', function ($uibModal, userFactory, authFactory, $timeout, $state, $location, $transitions, $interval, PageService) {

    let vm = this;
    vm.verify = false;
    vm.valid = true;
    vm.tel = '';
    vm.msg = '';
    vm.codeExpires = 120;

    vm.create = function () {

        let UserData = {
            name : vm.name,
            username : vm.tel.replace(/\+7/,''),
            tel : vm.tel.replace(/\+7/,''),
            email : vm.email,
            password : vm.password,
            ps: vm.password,
            defaultStorage: vm.storage
        };

        vm.open();

        userFactory.create(UserData).then(function (msg) {
          vm.msg = msg.data.message;
          console.log(vm.msg);

            if (msg.status === 'success') {
              vm.verify = true;
            }

          }, function (err) {
            console.log(err);
          });
    };

    vm.allUsers = function () {
        userFactory.all();
    };

    vm.getCurrentUser = function () {
        authFactory.getUser()
    };

    vm.open = function (size, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'myModalContent.html',
        controller: 'ModalController',
        controllerAs: 'mc',
        size: size,
        appendTo: parentElem,
        resolve: {
          items: function () {
            return vm.msg;
          }
        }
      });

      modalInstance.result.then(function (success) {
        if (success) {
          $timeout(function () {
            $state.go('auth.login');
          },2500);
        } else {
          vm.msg = 'Ошибка сервера, попробуйте позднее!';
        }
      }, function () {
        vm.msg = 'Ошибка сервера, попробуйте позднее!';
      });

    };

    onInit();

    function onInit() {
      PageService.getList()
        .then(function (res) {
          vm.list = res.data;
          console.log(vm.list, 'list is');
        })
    }

}];

export default userMainController;