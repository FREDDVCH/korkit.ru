'use strict'

const loginPage = function () {
    return {
        restrict: 'E',
        templateUrl: './client/user/auth/view/login.user.client.view.html'
    }
};

export default loginPage;
