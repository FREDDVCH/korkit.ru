'use strict';

const modalController = ['$uibModalInstance', 'items', 'User', '$interval', function ($uibModalInstance, items, userFactory, $interval) {

  var mc = this;
  mc.items = items;
  mc.status = false;
  mc.item = 'Введите полученный код';
  mc.code = '';
  mc.codeExpires = 120;

  function onInit() {
    $interval(function () {
      mc.codeExpires -= 1;
    },1000);
  }

  mc.ok = function () {
    console.log(mc.code);
    userFactory.sendCode({ code: mc.code }).then(function (res) {
      console.log(res);
      if (res.data.success){
        console.log(res, 'res res');
        console.log(res.data, 'res data');
        mc.status = res.data.success;
        $interval.cancel();
        $uibModalInstance.close(mc.status);
        return;
      }
      mc.status = res.success;
      mc.item = res.message;
      console.log(res.message);
    });
  };

  mc.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

  onInit();

}];

export default modalController;