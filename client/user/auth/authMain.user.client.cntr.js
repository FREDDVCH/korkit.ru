'use strict';
import '../../../services/auth';

const AuthMainController = ['$rootScope','$state','$location','User','Auth','$transitions',function ($rootScope, $state, $location, User, Auth, $transitions) {

    var vm = this;
    vm.check = false;

  vm.recoveryPass = function () {
    let number = vm.tel.replace(/\+7/,'');

    User.recovery(number)
      .then(function (msg) {
        vm.msg = msg.data;

        if (msg.status === 'success') {
          vm.verify = true;
        }

        if (msg.status === 'warning') {
          vm.verify = false;
        }
        vm.check = true;

      }, function (err) {
      });
  };

    vm.getUserInfo = function () {

      Auth.getUser().then(function (user) {

        User.getUser(user.data._id)
          .then(function (user) {
            vm.user = user.data[0];
            $rootScope.user = user.data[0];
          },function (err) {
          });

      });

    };

    vm.loggedIn =  Auth.isLoggedIn();


    $transitions.onStart({}, function($transition$) {

        vm.loggedIn = Auth.isLoggedIn();

        Auth.getUser()
            .then(function (data) {
                vm.user = data.data;
            });

    });

    vm.getUser = Auth.getUser();

    vm.doLogin = function () {
        vm.processing = true;
        vm.error = '';

        Auth.login(vm.loginData.username, vm.loginData.password)
            .then(function (data) {
                vm.processing = false;
                Auth.getUser()
                    .then(function (data) {
                        vm.user = data.data;
                        $state.go('home.main');
                    });
                if(data.success) {

                } else {

                    vm.error = data.message;
                }
            });
    };

    vm.doLogout = function () {
        Auth.logout();
        $state.go('auth.login');
    };

    vm.saveProfile = function () {
      Auth.putUser(vm.user).then(function (user) {
        $state.go('personal.page',{userId: vm.user._id});
      });
    };

    vm.cancelEdit = function () {
      $state.go('personal.page',{userId: vm.user._id});
    };

    vm.removeFromGarage = function (carIndex) {
      vm.user.garage = vm.user.garage.filter((car, index) => index !== carIndex);
    }
}];

export default AuthMainController;