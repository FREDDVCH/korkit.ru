'use strict';

const modalComponent = {
  templateUrl: 'myModalContent.html',
    bindings: {
    resolve: '<',
      close: '&',
      dismiss: '&'
  },
  controller: function () {
    var mc = this;

    mc.$onInit = function () {
      mc.success = mc.resolve.success;
    };

    mc.ok = function () {
      mc.close({$value: mc.success});
    };

    mc.cancel = function () {
      mc.dismiss({$value: 'cancel'});
    };
  }
};

export default modalComponent