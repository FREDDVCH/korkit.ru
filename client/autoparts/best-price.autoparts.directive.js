'use strict';

import bestPriceTemplate from './view/best-price.autoparts.view.html';

const bestPriceDirective = [function () {
  return {
    restrict: 'E',
    template:  bestPriceTemplate
  }
}];

export default bestPriceDirective;