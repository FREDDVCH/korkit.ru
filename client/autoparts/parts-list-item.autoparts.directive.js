'use strict';

import partListItemTemplate from './view/parts-list-item.autoparts.view.html'

const partListItem = [function () {
    return {
        restrict: 'E',
        template: partListItemTemplate,
        controller: 'searchController',
        controllerAs: 'sc'
    }
}];

export default partListItem;