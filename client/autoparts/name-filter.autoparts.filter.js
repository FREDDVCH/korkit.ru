'use strict';

const filter = function () {

  return function (input) {

    input = input || '';

    let regExp = /Склад/,
        out;
    if(input.toLowerCase().indexOf('нск') > 0)
      out = input.replace(input, 'КорКит');
    else out = input.replace(regExp,'');

    return out;

  }

};


export default filter;