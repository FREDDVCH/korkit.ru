'use strict';

import autoBrandTemplate from './view/auto-brand.autoparts.view.html'

const autoBrandDirective = [function () {
    return {
        restrict: 'E',
        template: autoBrandTemplate
    }
}];

export default autoBrandDirective;