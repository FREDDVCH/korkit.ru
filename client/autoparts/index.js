'use strict';

import autocatalogItem from './catalog-item.autoparts.directive'
import autocatalogDirective from './main-catatog.autoparts.directive'
import autocatalogModelListDirective from './models.autoparts.directive'
import autocatalogMainCntr from './autopartsMain.autoparts.cntr'
import autoBrandDirective from './auto-brand.autoparts.directive'
import resultSearchDirective from './result-search.autoparts.directive'
import partsListDirective from './parts-list.autoparts.directive'
import crossListDirective from './cross-list.autoparts.directive'
import partsListItemDirective from './parts-list-item.autoparts.directive'
import crossListItemDirective from './cross-list-item.autoparts.directive'
import searchMainCntr from './search-main.autoparts.cntr'
import bestPriceDirective from './best-price.autoparts.directive'
import NameFilter from './name-filter.autoparts.filter'
import suppliersList from "./suppliers-list.autoparts.directive";
import suppliersListItem from "./suppliers-list-item.autoparts.directive";
import suppliersCrossItem from "./suppliers-analogs-item.autoparts.directive"
//
// import mainCoreCntrl from "../core/main.core.cntr";
import './scss/autocatalog.scss'

const autocatalogModule = angular.module('Autocatalog', []);

autocatalogModule.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // $urlRouterProvider.otherwise('/');
    $stateProvider.state('autocatalog',{
        abstract: true,
        url: '/autocatalog',
        data : {
            title: 'Каталог транспортных средств'
        },
        template: '<ui-view>'
    });
    $stateProvider.state('autocatalog.main',{
        url: '/full',
        data : {
            title: 'Каталог транспортных средств'
        },
        template: '<auto-catalog></auto-catalog>'
    });
    $stateProvider.state('autocatalog.item',{
        url: '/:brand/:id',
        data : {
            title: 'Каталог транспортных средств'
        },
        template: '<auto-catalog-item></auto-catalog-item>'
    });
    $stateProvider.state('search',{
        url: '/search?q',
        data : {
            title: 'Поиск по VIN'
        },
        template: '<search-part-page></search-part-page>'
    });
}]);

autocatalogModule.controller('AutocatalogMainCntr', autocatalogMainCntr);
autocatalogModule.controller('searchController', searchMainCntr);
autocatalogModule.filter('nameFilter', NameFilter);
autocatalogModule.directive('searchPartPage', resultSearchDirective);
autocatalogModule.directive('bestPrice', bestPriceDirective);
autocatalogModule.directive('autoBrand', autoBrandDirective);
autocatalogModule.directive('partsList', partsListDirective);
autocatalogModule.directive('crossList', crossListDirective);
autocatalogModule.directive('suppliersList', suppliersList);
autocatalogModule.directive('suppliersListItem', suppliersListItem);
autocatalogModule.directive('suppliersCrossItem', suppliersCrossItem);
autocatalogModule.directive('partsListItem', partsListItemDirective);
autocatalogModule.directive('crossListItem', crossListItemDirective);
autocatalogModule.directive('autoCatalog', autocatalogDirective);
autocatalogModule.directive('autoCatalogItem', autocatalogItem);
autocatalogModule.directive('autoCatalogModelList', autocatalogModelListDirective);
export default autocatalogModule;