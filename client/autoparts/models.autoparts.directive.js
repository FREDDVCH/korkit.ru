'use strict';

import templateModels from './view/models.autoparts.view.html'

const mainModelsDirective = ['$timeout',function ($timeout) {

   return {
       scope:{
           link:'=',
           cat:'='
       },
       link: function (scope) {


           scope.models = [];
           arrayModels(1);

           function arrayModels(cat) {


               let array = [
                   {
                       brand:'Audi',
                       id: 1
                   },
                   {
                       brand:'BMW',
                       id: 2
                   },
                   {
                       brand:'Toyota',
                       id: 3
                   },
                   {
                       brand:'Subaru',
                       id: 4
                   },
                   {
                       brand:'Kia',
                       id: 5
                   },
                   {
                       brand:'Noname',
                       id: 6
                   },
                   {
                       brand:'Lada',
                       id: 7
                   },
                   {
                       brand:'Bentley',
                       id: 8
                   },
                   {
                       brand:'Ford',
                       id: 9
                   },
                   {
                       brand:'Lancer',
                       id: 10
                   }
               ];
               let arrayHeavy = [
                   {
                       brand:'Lifan',
                       id: 1
                   },
                   {
                       brand:'MAN',
                       id: 2
                   },
                   {
                       brand:'Some',
                       id: 3
                   },
                   {
                       brand:'Heavy',
                       id: 4
                   },
                   {
                       brand:'CARd',
                       id: 5
                   }
               ];
               let arrayAll = [
                   {
                       brand:'Lifan',
                       id: 11
                   },
                   {
                       brand:'MAN',
                       id: 12
                   },
                   {
                       brand:'Some',
                       id: 13
                   },
                   {
                       brand:'Heavy',
                       id: 14
                   },
                   {
                       brand:'CARd',
                       id: 15
                   },
                   {
                       brand:'Audi',
                       id: 1
                   },
                   {
                       brand:'BMW',
                       id: 2
                   },
                   {
                       brand:'Toyota',
                       id: 3
                   },
                   {
                       brand:'Subaru',
                       id: 4
                   },
                   {
                       brand:'Kia',
                       id: 5
                   },
                   {
                       brand:'Noname',
                       id: 6
                   },
                   {
                       brand:'Lada',
                       id: 7
                   },
                   {
                       brand:'Bentley',
                       id: 8
                   },
                   {
                       brand:'Ford',
                       id: 9
                   },
                   {
                       brand:'Lancer',
                       id: 10
                   }
               ];

               $timeout(function () {
                   scope.modelsAll = array;
               },1500)
           }

       },
       restrict:'E',
       template:templateModels
   }
}];

export default mainModelsDirective