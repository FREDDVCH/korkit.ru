'use strict';

import SuppliersCrossItemTemplate from './view/suppliers-analogs-item.autoparts.view.html';

const suppliersCrossItem = [function () {
  return {
    restrict: 'E',
    template: SuppliersCrossItemTemplate
  }
}];

export default suppliersCrossItem