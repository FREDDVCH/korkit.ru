'use sctrict';

import mainCatalogDirective from './view/main-catalog.autoparts.view.html'

const mainCatalog = [function () {
    return {
        restrict: 'E',
        template: mainCatalogDirective
    }
}];

export default mainCatalog;