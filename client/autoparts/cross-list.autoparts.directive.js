'use strict';

import crossListTemplate from './view/cross-list.autoparts.view.html'

const crossList = [function () {
  return {
    restrict: 'E',
    template: crossListTemplate
  }
}];

export default crossList