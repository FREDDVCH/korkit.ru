'use strict';

import SuppliersListTemplate from './view/suppliers-list.autoparts.view.html'

const suppliersList = [function () {
  return {
    restrict: 'E',
    template: SuppliersListTemplate
  }
}];

export default suppliersList