'use strict';

import partListTemplate from './view/parts-list.autoparts.view.html'

const partList = [function () {
   return {
       restrict: 'E',
       template: partListTemplate,
       controller: 'searchController',
       controllerAs: 'sc'
   }
}];

export default partList;