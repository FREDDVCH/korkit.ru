'use strict';

import autocatalogItemTemplate from './view/item.autoparts.view.html'

const autocatalogItemDirective = [ function () {
    return {
        restrict: 'E',
        scope: {
            id: '='
        },
        template: autocatalogItemTemplate,
        controller: 'AutocatalogMainCntr',
        controllerAs: 'vm'
    }
}];

export default autocatalogItemDirective;