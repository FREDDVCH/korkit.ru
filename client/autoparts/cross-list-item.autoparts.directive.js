'use strict';

import crossListItemTemplate from './view/cross-list-item.autoparts.view.html';

const crossListItem = [function () {
  return {
    restrict: 'E',
    template: crossListItemTemplate
  }
}];

export default crossListItem