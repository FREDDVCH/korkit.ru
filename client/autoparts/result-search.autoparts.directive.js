'use strict';

import resultTemplate from './view/result-search.autoparts.view.html'

const resultSearch = [function () {
    return {
        restrict: 'E',
        template: resultTemplate,
        controller: 'searchController',
        controllerAs: 'sc',
        link: function (scope, element, args) {

          $(document).on("scroll", function(e){

              if($(document).scrollTop() > 800) {
                $('.scroll-top-btn').css('opacity', 1)
              } else {
                $('.scroll-top-btn').css('opacity', 0);
              }

          });

          $('.scroll-top-btn').on('click', function (e) {

          (function myLoop (i) {
            setTimeout(function () {
              let scrollTopHeight = $(document).scrollTop()/i,
                scrollTop = $(document).scrollTop() - scrollTopHeight;

              $(document).scrollTop(scrollTop);
              if (--i) myLoop(i);
            }, 15)
          })(50);


          });



        }
    }
}];

export default resultSearch;