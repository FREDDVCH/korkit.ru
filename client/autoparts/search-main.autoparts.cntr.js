'use strict';

const searchController = ['$timeout','$rootScope','$scope', '$window','$http','$stateParams','SearchParts','localStorageService','SupportService','User', 'Auth', function($timeout,$rootScope, $scope, $window, $http, $stateParams, SearchParts, localStorageService, SupportService, User, Auth) {


    let params = $stateParams.q;
    var sc = this;
    var regexp = /[а-яА-Я]/;
    sc.vin = params.toString();
    sc.loaderVIN = false;
    sc.loaderCross = false;
    sc.loaderBestDeal = false;
    sc.loaderSuppliers = false;
    sc.badRequest = false;
    sc.progressValue = 0;
    sc.hiddenLoader = true;

    sc.animation = function (e, item, storage, count, array = 'supplCross') {
      if (storage.quantity !== 0 || storage.count !== 0) {
        let orderItem = {orderCount: count, ...item, orderStorage: {...storage}};
        localStorageService.setItem(orderItem);
        var basket = JSON.parse(localStorage.getItem('basket')),
        countItem = '';
        if (basket && basket.length > 0) {
          var basketItem = basket.find(function (el) {
            if (array !== 'parts' && array !== 'partsCross') {
              if (el.vin === item.vin && el.name === item.name) {

                return true;
              }
            }
            if (array === 'parts' || array === 'partsCross') {


              if (el.vin === item.vin && el.name === item.name && el.orderStorage._id === storage._id) {

                return true;
              }
            }
          });

          countItem = basketItem.orderCount;

          if (array === 'supplCross') {
            sc.suppliersCross.forEach(function (data, index) {

              if (data.vin === item.vin && data.name === item.name) {
                data.stocks.forEach(function (st, ind) {
                  if (st.warehouse.id === storage.warehouse.id) {
                    if (sc.suppliersCross[index].stocks[0].quantity !== 0) {
                      sc.suppliersCross[index].stocks[0].quantity = sc.suppliersCross[index].stocks[0].quantity - count;
                      $(e.currentTarget).removeClass('btn-success');
                      $(e.currentTarget).addClass('btn-primary');
                      $(e.currentTarget).find('i').removeClass('fa-cart-plus');
                      $(e.currentTarget).find('span').text('В корзине ' + countItem);
                    } else {
                      $(e.currentTarget).attr('ng-click', 'vm.action()');
                      $(e.currentTarget).find('span').text('Заказать');
                    }
                  }
                })
              }
            });
          } else if (array === 'supplier') {
            sc.suppliers.forEach(function (data, index) {
              if (data.vin === item.vin && data.name === item.name) {
                data.stocks.forEach(function (st, ind) {
                  if (st.warehouse.id === storage.warehouse.id) {
                    if (sc.suppliers[index].stocks[0].quantity !== 0) {
                      sc.suppliers[index].stocks[0].quantity = sc.suppliers[index].stocks[0].quantity - count;
                      $(e.currentTarget).removeClass('btn-success');
                      $(e.currentTarget).addClass('btn-primary');
                      $(e.currentTarget).find('i').removeClass('fa-cart-plus');
                      $(e.currentTarget).find('span').text('В корзине ' + countItem);
                    } else {
                      $(e.currentTarget).attr('ng-click', 'vm.action()');
                      $(e.currentTarget).find('span').text('Заказать');
                    }
                  }
                })
              }
            });
          } else if (array === 'parts') {


            sc.parts.forEach(function (data, index) {
              if (data.vin === item.vin && data.name === item.name) {
                data.stocks.forEach(function (st, ind) {

                  if (st._id === storage._id) {
                    if (sc.parts[index].stocks[ind].count !== 0) {
                      sc.parts[index].stocks[ind].count = sc.parts[index].stocks[ind].count - count;
                      $(e.currentTarget).removeClass('btn-success');
                      $(e.currentTarget).addClass('btn-primary');
                      $(e.currentTarget).find('i').removeClass('fa-cart-plus');
                      $(e.currentTarget).find('span').text('В корзине ' + countItem);
                    } else {
                      $(e.currentTarget).attr('ng-click', 'vm.action()');
                      $(e.currentTarget).find('span').text('Заказать');
                    }
                  }

                })
              }
            });

          } else if (array === 'partsCross') {
            sc.cross.forEach(function (data, index) {
              if (data.vin === item.vin && data.name === item.name) {
                data.stocks.forEach(function (st, ind) {

                  if (st._id === storage._id) {
                    if (sc.cross[index].stocks[ind].count !== 0) {

                      sc.cross[index].stocks[ind].count = sc.cross[index].stocks[ind].count - count;
                      $(e.currentTarget).removeClass('btn-success');
                      $(e.currentTarget).addClass('btn-primary');
                      $(e.currentTarget).find('i').removeClass('fa-cart-plus');
                      $(e.currentTarget).find('span').text('В корзине ' + countItem);
                    } else {
                      $(e.currentTarget).attr('ng-click', 'vm.action()');
                      $(e.currentTarget).find('span').text('Заказать');
                    }
                  }

                })
              }
            });

          }
        }
      } else {
        alert('запчасть кончилась!');
      }
    };

    sc.addItemToBasket = function (item,storage,count) {
      var el = 'add' + '_' + item._id;
      var domEl = angular.element('#' + el);
      domEl.text('В корзине');
      let orderItem = {orderCount: count, ...item, orderStorage: { ...storage } };
      localStorageService.setItem(orderItem);
    };

    sc.modalMove = function (event) {

      // let window = angular.element(window);
      let element = angular.element('.modal-address');

      let x = event.clientX,
          y = event.clientY,
          width = $(window).width()/2,
          height = $(window).height(),
          style = {
            top: y+350,
            left: width
          };

      element.css(style);

    };

    sc.showLocation = function (location) {
      let element = angular.element('.modal-address');

      sc.message = {};

      if (location.toLowerCase().indexOf('нск') > 0) {
        sc.message = {
          message: '',
          address: 'г. Новосибирск: ул Николая Островского, 120 (на пересечении ул Ипподромской и ул. Писарева, вход со стороны ул.Писарева.'
        }
      } else if (location.toLowerCase().indexOf('евроавто') > 0) {
        sc.message = {
          message: '',
          address: 'г. Бийск, Алтайский край: ул. Мерлина, 11'
        }
      } else if (location.toLowerCase().indexOf('автолавка') > 0) {
        sc.message = {
          message: '',
          address: 'г. Бийск, Алтайский край: ул. Советская, 60'
        }
      }

      element.text(sc.message.message);

    };

    sc.progress = function () {
      const progressValueInt = setInterval(function () {
        sc.progressValue++;

        if(sc.progressValue === 100) {
          clearInterval(progressValueInt);
          angular.element('.loader').addClass('hidden-loader');
          $timeout(function () {
            sc.hiddenLoader = false;
          }, 500)
        }
      }, 200);
    };


    sc.hideLocation = function () {
      // sc.message = null;
    };
    if (!regexp.exec(params)) {
      sc.badRequest = false;
    if(Auth.isLoggedIn()) {
      User.me()
        .then(function (res) {
          return res.data;
        })
        .then(function (res) {
          return User.getUser(res._id);
        })
        .then( function (res) {
          SearchParts.getItemByVIM(params, $rootScope.user.percent).then(function (data) {
            sc.parts = data.res;
            sc.loaderVIN = true;
          });

          SearchParts.getCrossList(params, $rootScope.user.percent).then(function (data) {
            sc.cross = data.res;
            sc.loaderCross = true;
          });

          SearchParts.getCheap(params, $rootScope.user.percent).then(function (cheap) {
            sc.cheap = cheap.res;
            sc.loaderBestDeal = true;
          });

          SearchParts.getSupplier(params, $rootScope.user.percent).then(function (data) {

            sc.suppliers = data.res;
            sc.loaderSuppliers = true;
          });

          SearchParts.getSupplierCross(params, $rootScope.user.percent).then(function (data) {

            if (data.res.status === 'mismatch') {
              sc.oldSuppliersCross = data.res;
              sc.suppliersCross = data.res;
              sc.suppliersCross.body = sc.suppliersCross.body.filter( item => item );
            } else {
              sc.suppliersCross = data.res;
              sc.loaderSuppliersCross = true;
            }
          });
          sc.returnToBrandList = function () {
            sc.statusMessage = null;
            sc.loaderSuppliersCross = false;
            sc.suppliersCross = sc.oldSuppliersCross;
          };
          sc.refreshList = function (item) {
            sc.loaderSuppliersCross = false;
            sc.statusMessage = 'Запрос обрабатывается';

            SearchParts.getSupplierCross(item.article, $rootScope.user.percent, item.brand)
              .then(function (data) {
                if (data.res[0].vin) {
                  sc.suppliersCross = data.res;
                  sc.loaderSuppliersCross = true;
                  sc.statusMessage = `Запрос обработан успешно! Найдено ${data.res.length} поз.`;
                } else {
                  throw e;
                }
              }).catch(function (e) {
              sc.loaderSuppliersCross = false;
              sc.statusMessage = `Запрос обработан. Аналоги производителя ${item.brand} запчасти не найдены!`;
            })
          };

        });

    } else {
      $rootScope.user = {};
      $rootScope.user.percent = 0;
      sc.loaderSuppliersCross = true;
      sc.loaderSuppliers = true;
      SearchParts.getItemByVIM(params, $rootScope.user.percent).then(function (data) {
        sc.parts = data.res;
        sc.loaderVIN = true;
        checkExist(sc.parts);
      });
      SearchParts.getCrossList(params, $rootScope.user.percent).then(function (data) {
        sc.cross = data.res;
        sc.loaderCross = true;
        checkExist(sc.cross);
      });
      SearchParts.getCheap(params, $rootScope.user.percent).then(function (cheap) {
        sc.cheap = cheap.res;
        sc.loaderBestDeal = true;
      });
      SearchParts.getSupplier(params, $rootScope.user.percent).then(function (data) {

        sc.suppliers = data.res;
        sc.loaderSuppliers = true;
        checkExist(sc.suppliers);
      });
      SearchParts.getSupplierCross(params, $rootScope.user.percent).then(function (data) {

        if (data.res.status === 'mismatch') {
          sc.oldSuppliersCross = data.res;
          sc.suppliersCross = data.res;
          checkExist(sc.suppliersCross);
          sc.suppliersCross.body = sc.suppliersCross.body.filter( item => item );
        } else {
          sc.suppliersCross = data.res;
          sc.loaderSuppliersCross = true;
        }

      });

      sc.returnToBrandList = function () {
        sc.statusMessage = null;
        sc.loaderSuppliersCross = false;
        sc.suppliersCross = sc.oldSuppliersCross;
      };
      sc.refreshList = function (item) {
        sc.loaderSuppliersCross = false;
        sc.statusMessage = 'Запрос обрабатывается';

        SearchParts.getSupplierCross(item.article, $rootScope.user.percent, item.brand)
          .then(function (data) {

            if (data.res[0].vin) {
              sc.suppliersCross = data.res;
              checkExist(sc.suppliersCross);
              sc.loaderSuppliersCross = true;
              sc.statusMessage = `Запрос обработан успешно! Найдено ${data.res.length} поз.`;
            } else {
              throw e;
            }
          }).catch(function (e) {
          sc.loaderSuppliersCross = false;
          sc.statusMessage = `Запрос обработан. Аналоги производителя ${item.brand} запчасти не найдены!`;
        })
      }
      }
    } else {
      sc.badRequest = true;
    }

    function checkExist(arr, init = true) {
      try {
        if (arr && JSON.parse(localStorage.getItem('basket'))) {

          var basket = JSON.parse(localStorage.getItem('basket')),
            countItem = '';
          arr.forEach(function (item, index) {
            basket.forEach(function (el, i) {
              if (item.name === el.name && item.vin === el.vin) {

                item.stocks.forEach(function (stock, index) {
                  if (el.orderStorage.warehouse) {
                    if (el.orderStorage.warehouse['id'] === stock.warehouse['id']) {
                      item.basket = el.orderCount;
                      if (init) {
                        stock.quantity -= el.orderCount;
                      } else {
                        stock.quantity -= 1;
                      }
                    }
                  } else {


                    if (el.orderStorage._id === stock._id && el.name === item.name) {


                      stock.basket = el.orderCount;
                      if (init) {
                        stock.count -= el.orderCount;
                      } else {
                        stock.count -= 1;
                      }
                    }
                  }


                });
              }
            })

          });

        }
      } catch(e) {
        console.log('Обработка данных...')
      }


    }

    // function basketCheckOnPriceChange() {
    //   var basket = JSON.parse(localStorage.getItem('basket'));
    //
    //   basket.forEach(function (element, index) {
    //     var vin = element.vin,
    //       storage = element.orderStorage,
    //       price = element.orderStorage.price;
    //
    //     if(Auth.isLoggedIn()) {
    //
    //       SearchParts.getItemByVIM(vin, $rootScope.user.percent).then(function (res) {
    //         var data = res.data;
    //
    //         data.forEach(function (item) {
    //           if (vin === item.vin && element.brand === item.brand) {
    //             item.stocks.forEach(function (stock) {
    //               if (stock.name === element.orderStorage.name) {
    //
    //               }
    //             })
    //           }
    //         })
    //
    //       });
    //
    //
    //     } else {
    //       SearchParts.getItemByVIM(vin, $rootScope.user.percent).then(function (res) {
    //         var data = res.data;
    //
    //         data.forEach(function (item) {
    //           if (vin === item.vin && element.brand === item.brand) {
    //             item.stocks.forEach(function (stock) {
    //               if (stock.name === element.orderStorage.name) {
    //
    //               }
    //             })
    //           }
    //         })
    //
    //       });
    //
    //
    //     }
    //   })
    // }
    
    function onInit() {
      // basketCheckOnPriceChange();

    }

    onInit();
}];

export default searchController;