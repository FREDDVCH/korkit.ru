'use strict';

import SuppliersListItemTemplate from './view/suppliers-list-item.autoparts.view.html';

const suppliersListItem = [function () {
  return {
    restrict: 'E',
    template: SuppliersListItemTemplate
  }
}];

export default suppliersListItem