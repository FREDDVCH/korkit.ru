'use strict';
//add all core depends
import 'angular';
import '@uirouter/angularjs';
import 'angular-animate';
import 'angular-ui-bootstrap';

import  './../services/auth';
import '../services/parts'
import './core';
import './autoparts'
import './news'
import './parts/parts.module';
import './user/auth';
import './user/personal';
import './components';
import './catalogs';

//connect main app parts